<?php

use App\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password', 60)->nullable();
            $table->string('role');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->enum('gender',User::$SEX);
            $table->decimal('weight', 5, 2)->unsigned()->nullable();
            $table->date('birthday');
            $table->integer('height')->unsigned()->nullable();
            $table->text('picture')->nullable();
            $table->enum('unit',User::$UNIT)->nullable();
            $table->string('onesignal_id')->nullable();
            $table->boolean('verified')->default(false);
            $table->string('token')->nullable();
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
