<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('route');
            $table->integer('calories')->unsigned()->nullable();
            $table->double('distance',15,8)->unsigned()->nullable();
            $table->integer('duration')->unsigned()->nullable();
            $table->decimal('average_speed', 8, 2)->unsigned()->nullable();
            $table->decimal('average_pace', 8, 2)->unsigned()->nullable();
            $table->decimal('max_speed', 8, 2)->unsigned()->nullable();
            $table->decimal('max_pace', 8, 2)->unsigned()->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trainings');
    }
}
