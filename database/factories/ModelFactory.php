<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\GroupTraining;
use Carbon\Carbon;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->safeEmail,
        'role' => 'user',
        'first_name' => $faker->firstName,
        'last_name' => $faker ->lastName,
        'gender' => $faker->randomElement($array = array ('male', 'female')),
        'weight' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 999),
        'height' => $faker->numberBetween(0,300),
        'unit' => $faker->randomElement($array = array ('km', 'mi')),
        'birthday' => $faker->date('Y-m-d'),
        'picture' => 'https://robohash.org/'.$faker->word.'.png?size=300x300',
        'password' => bcrypt('qweqwe'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Sport::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->colorName,
        'title_eng' =>$faker->safeColorName,
        'picture' => 'https://robohash.org/'.$faker->word.'.png?size=72x72',
    ];
});

$factory->define(App\Training::class, function (Faker\Generator $faker) {
    return [
        'user_id' => '1',
        'route' => json_encode(array(
            array("lat" => 43.76, "lng" => 76.77),
            array("lat" => 41.76, "lng" => 76.77),
//            array("lat" => 47.76, "lng" => 73.77)
        )),
        'calories' => $faker->numberBetween(0, 10000),
        'sport_id' => $faker->numberBetween(1, 15),
        'distance' => $faker->randomFloat(2, 0, 10000),
        'duration' => $faker->numberBetween(0, 10000),
        'average_speed' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 999),
        'average_pace' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 999),
        'max_speed' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 99999),
        'max_pace' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 99999),
        'notes' => $faker->text(200),
    ];
});

$factory->define(App\GroupTraining::class, function (Faker\Generator $faker) {
    return [
        'sport_id' => '1',
        'user_id' => '1',
        'location' =>
            array(
                "lat" => $faker->randomFloat(2, -90, 90),
                "lon" => $faker->randomFloat(2, -180, 180)
            ),

        'title' => $faker->text(50),
        'description' => $faker->text(120),
        'price' => $faker->numberBetween(0, 5000),
        'private_state' => GroupTraining::PRIVATE_STATE_FRIENDS,
        'start_date' => $faker->date('Y-m-d'),
        'start_time' => '9:30',
        'end_time' => '19:30',
        'repeat_type' => GroupTraining::REPEAT_TYPE_NONE,

    ];
});

$factory->define(App\Invitation::class, function (Faker\Generator $faker) {
    return [
        'group_training_id' => '1',
        'sender_id' => '1',
        'receiver_id' => '1',
        'state' => 'new',
        'expires_at' => Carbon::now(),
    ];
});

$factory->define(App\Friend::class, function (Faker\Generator $faker) {
    return [
        'sender_id' => '1',
        'receiver_id' => '2',
        'status' => 'requested',
    ];
});


$factory->define(App\Route::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->text(20),
        'private_state' => App\Route::PRIVATE_STATE_FRIENDS,
        'picture' => 'https://robohash.org/'.$faker->word.'.png?size=300x300',
        'distance' => $faker->randomFloat(2, 0, 10000)
    ];
});

$factory->define(App\Setting::class, function (Faker\Generator $faker) {
    return [
        'invitation_notifications' => '1',
        'friend_request_notifications' => '1'
    ];
});


