<?php

use Illuminate\Database\Seeder;

class SportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sports')->insert([
            [
                'title' => 'Аэробика',
                'title_eng' => 'Aerobics',
                'calories' => '455',
                'picture' => asset('images/sports/aerobics.png')
            ],
            [
                'title' => 'Спуск на лыжах',
                'title_eng' => 'Backcountry skiing',
                'calories' => '680',
                'picture' => asset('images/sports/backcountry_skiing.png')
            ],
            [
                'title' => 'Бэдмингтон',
                'title_eng' => 'Badminton',
                'calories' => '238',
                'picture' => asset('images/sports/badmington.png')
            ],
            [
                'title' => 'Бейсбол',
                'title_eng' => 'Baseball',
                'calories' => '347',
                'picture' => asset('images/sports/baseball.png')
            ],
            [
                'title' => 'Бокс',
                'title_eng' => 'Boxing',
                'calories' => '408',
                'picture' => asset('images/sports/boxing.png')
            ],
            [
                'title' => 'Бег на лыжах',
                'title_eng' => 'Cross country skiing',
                'calories' => '544',
                'picture' => asset('images/sports/cross_country_skiing.png')
            ],
            [
                'title' => 'Сycling',
                'title_eng' => 'Cycling',
                'calories' => '476',
                'picture' => asset('images/sports/cycling.png')
            ],
            [
                'title' => 'Спуск на велосипеде',
                'title_eng' => 'Downhill cycling',
                'calories' => '376',
                'picture' => asset('images/sports/downhill_cycling.png')
            ],
            [
                'title' => 'Эллиптический тренажер',
                'title_eng' => 'Elliptical',
                'calories' => '612',
                'picture' => asset('images/sports/elliptical.png')
            ],
            [
                'title' => 'Фитнес',
                'title_eng' => 'Fitness',
                'calories' => '599',
                'picture' => asset('images/sports/fitness.png')
            ],
            [
                'title' => 'Гольф',
                'title_eng' => 'Golf',
                'calories' => '204',
                'picture' => asset('images/sports/golf.png')
            ],
            [
                'title' => 'Гандбол',
                'title_eng' => 'Handball',
                'calories' => '816',
                'picture' => asset('images/sports/handball.png')
            ],
            [
                'title' => 'Ходьба',
                'title_eng' => 'Hiking',
                'calories' => '408',
                'picture' => asset('images/sports/hiking.png')
            ],
            [
                'title' => 'Каякинг',
                'title_eng' => 'Kayaking',
                'calories' => '340',
                'picture' => asset('images/sports/kayaking.png')
            ],
            [
                'title' => 'Езда на велосипеде (горный)',
                'title_eng' => 'Riding mountain bike',
                'calories' => '578',
                'picture' => asset('images/sports/mountain_bike.png')
            ],
            [
                'title' => 'Другое',
                'title_eng' => 'Other',
                'calories' => '0',
                'picture' => asset('images/sports/other.png')
            ],
            [
                'title' => 'Ракетбол',
                'title_eng' => 'Racquetball',
                'calories' => '476',
                'picture' => asset('images/sports/racquetball.png')
            ],
            [
                'title' => 'Езда на велосипеде',
                'title_eng' => 'Riding a bicycle',
                'calories' => '272',
                'picture' => asset('images/sports/riding_bicycle.png')
            ],
            [
                'title' => 'Езда на велосипеде (дорога)',
                'title_eng' => 'Road cycling ',
                'calories' => '680',
                'picture' => asset('images/sports/road_bike.png')
            ],
            [
                'title' => 'Гребля',
                'title_eng' => 'Rowing',
                'calories' => '476',
                'picture' => asset('images/sports/rowing.png')
            ],
            [
                'title' => 'Бег',
                'title_eng' => 'Running',
                'calories' => '850',
                'picture' => asset('images/sports/running.png')
            ],
            [
                'title' => 'Катание на коньках',
                'title_eng' => 'Skating',
                'calories' => '476',
                'picture' => asset('images/sports/skating.png')
            ],
            [
                'title' => 'Футбол',
                'title_eng' => 'Soccer',
                'calories' => '476',
                'picture' => asset('images/sports/soccer.png')
            ],
            [
                'title' => 'Плавание',
                'title_eng' => 'Swimming',
                'calories' => '476',
                'picture' => asset('images/sports/swimming.png')
            ],
            [
                'title' => 'Настольный теннис',
                'title_eng' => 'Table tennis',
                'calories' => '272',
                'picture' => asset('images/sports/table_tennis.png')
            ],
            [
                'title' => 'Волейбол',
                'title_eng' => 'Volleyball',
                'calories' => '204',
                'picture' => asset('images/sports/volleyball.png')
            ],
            [
                'title' => 'Просмотр спортивных матчей',
                'title_eng' => 'Watching sports matches',
                'calories' => '70',
                'picture' => asset('images/sports/watching_tv.png')
            ],
            [
                'title' => 'Бодибилдинг',
                'title_eng' => 'Weightlifting',
                'calories' => '430',
                'picture' => asset('images/sports/weightlifting.png')
            ],
            [
                'title' => 'Йога',
                'title_eng' => 'Yoga',
                'calories' => '170',
                'picture' => asset('images/sports/yoga.png')
            ],
        ]);

    }
}
