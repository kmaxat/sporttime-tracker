<?php

use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SportsSeeder::class);
        factory(App\User::class)->create(['email' => 'qwe@qwe.com']);
        factory(App\User::class)->create(['email' => 'kmaxat@gmail.com', 'role' => User::ROLE_ADMIN]);
        factory(App\User::class, 4)->create()->each(function($u){
            $u->trainings()->save(factory(App\Training::class)->create());
            $u->settings()->save(factory(App\Setting::class)->make());
        });
    }
}
