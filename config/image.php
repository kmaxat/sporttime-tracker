<?php

return array(
    'library'     => 'gd',
    'upload_dir'  => 'uploads',
    'assets_upload_path' => 'public/images/',
    'quality'     => 85,
    'dimensions'  => [
        ['2048','2048',true, 85, 'original'],
    ]
);