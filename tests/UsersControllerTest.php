<?php

use App\User;
use Elasticquent\ElasticquentTrait;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class UsersControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    use DatabaseTransactions;

    public function testGetMe()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        //Act & Assert
        $token = JWTAuth::fromUser($user);
        $request = $this->get('api/v1/users/me',
            ['HTTP_Authorization' => 'Bearer ' . $token])
            ->seeJson( ['email' => $user->email]);

    }

    public function testPutMe()
    {
        $user = factory(App\User::class)->create();
        $token = JWTAuth::fromUser($user);
        $request = $this->post('api/v1/users/me',
            ['birthday' => '1980-01-13'],
            ['HTTP_Authorization' => 'Bearer ' . $token]);
        $data = json_decode($request->response->getContent());
        $this->assertResponseOk();
        $this->assertEquals('1980-01-13', $data->birthday);
    }

    public function testUpdatePassword()
    {
        $user = factory(App\User::class)->create();
        $token = JWTAuth::fromUser($user);
        $request = $this->post('api/v1/users/me/password',
            [
                'password_old' => 'qweqwe',
                'password_new' => 'qweqweqwe'],
            [
                'HTTP_Authorization' => 'Bearer ' . $token
            ])->assertResponseOk();
    }

    public function testGetSearch()
    {
        //Arrange
        Artisan::call('index:delete');
        Artisan::call('index:create');

        $user = factory(App\User::class, 3)->create();
        $user[0]->first_name = 'Mariane';
        $user[1]->first_name = 'Mariane';
        $user[2]->first_name = 'Mariane';

        $user[0]->save();
        $user[0]->addToIndex();

        $user[1]->save();
        $user[1]->addToIndex();

        $user[2]->save();
        $user[2]->addToIndex();

        //Needed to wait for changes in ES. Otherwise will return empty results
        sleep(1);
        //Act & Assert
        $token = JWTAuth::fromUser($user[0]);
        $response = $this->call('GET', 'api/v1/users/search', ['q' => 'Mariane'], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);
        $searchResults = json_decode($response->getContent())->data;

        $this->assertResponseOk();
        $this->assertCount(2, $searchResults);
        $this->assertEquals($user[1]->first_name, $searchResults[0]->first_name);
        $this->assertEquals($user[2]->first_name, $searchResults[1]->first_name);
    }

    public function testGetUser()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        //Act & Assert
        $token = JWTAuth::fromUser($user);
        $response = $this->call('GET', 'api/v1/users/'.$user->id, [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $userJSON = json_decode($response->getContent());
        $this->assertEquals($user->id, $userJSON->id);
    }
}
