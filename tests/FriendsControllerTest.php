<?php

use App\Friend;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FriendsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseTransactions;

    public function testGetMeFriends()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $friend = factory(App\User::class)->create();
        $friendRequest = factory(App\Friend::class)->make();
        $friendRequest->sender_id = $user->id;
        $friendRequest->receiver_id = $friend->id;
        $friendRequest->status = Friend::ACCEPTED;
        $friendRequest->save();

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('GET', '/api/v1/friends/me', [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        //Assert
        $friends = json_decode($response->getContent())->data;
        $this->assertCount(1, $friends);
        $this->assertEquals($friend->email, $friends[0]->email);

    }

    public function testPostFriendshipRequest()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $friend = factory(App\User::class)->create();

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('POST', '/api/v1/friends/'.$friend->id, ['decision'=> 'requested'], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $this->seeInDatabase('friends',['sender_id' => $user->id, 'receiver_id' => $friend->id,
            'status' => Friend::REQUESTED]);

        //Assert
        $this->assertResponseOk();
    }
}
