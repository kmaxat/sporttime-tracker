<?php

use App\Record;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoutesControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetRoutes()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $route = factory(App\Route::class)->make();
        $user->routes()->save($route);

        $privateRoute = factory(App\Route::class)->make();
        $privateRoute->private_state = App\Route::PRIVATE_STATE_CLOSE;
        $user->routes()->save($privateRoute);

        $admin = factory(App\User::class)->make();
        $admin->role = User::ROLE_ADMIN;
        $admin->save();

        $adminRoute = factory(App\Route::class)->make();
        $admin->routes()->save($adminRoute);

        //Act & Assert
        $token = JWTAuth::fromUser($user);
        $this->get('api/v1/routes',
            ['HTTP_Authorization' => 'Bearer ' . $token]);

        $response = json_decode($this->response->getContent())->data;
        $this->assertResponseOk();
        $this->assertCount(2, $response);
        $this->assertEquals($route->id, $response[0]->id);
        $this->assertEquals($adminRoute->id, $response[1]->id);
    }

    public function testGetRoutesMe()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $route = factory(App\Route::class)->make();
        $user->routes()->save($route);

        $privateRoute = factory(App\Route::class)->make();
        $privateRoute->private_state = App\Route::PRIVATE_STATE_CLOSE;
        $user->routes()->save($privateRoute);

        //Act & Assert
        $token = JWTAuth::fromUser($user);
        $this->get('api/v1/routes/me',
            ['HTTP_Authorization' => 'Bearer ' . $token]);

        $response = json_decode($this->response->getContent())->data;
        $this->assertResponseOk();
        $this->assertCount(2, $response);
    }

    public function testGetRoute()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $route = factory(App\Route::class)->make();
        $user->routes()->save($route);

        $sportOne = factory(App\Sport::class)->create();
        $sportTwo = factory(App\Sport::class)->create();

        $recordSportOne = Record::create([
            'user_id' => $user->id,
            'sport_id' => $sportOne->id,
            'route_id' => $route->id,
            'duration_seconds' => 400,
        ]);

        $recordSportOneBig = Record::create([
            'user_id' => $user->id,
            'sport_id' => $sportOne->id,
            'route_id' => $route->id,
            'duration_seconds' => 2300,
        ]);

        $recordSportTwo = Record::create([
            'user_id' => $user->id,
            'sport_id' => $sportTwo->id,
            'route_id' => $route->id,
            'duration_seconds' => 500,
        ]);

        $recordSportTwoBig = Record::create([
            'user_id' => $user->id,
            'sport_id' => $sportTwo->id,
            'route_id' => $route->id,
            'duration_seconds' => 510,
        ]);

        //Act & Assert
        $token = JWTAuth::fromUser($user);
        $this->get('api/v1/routes/'.$route->id,
            ['HTTP_Authorization' => 'Bearer ' . $token]);

        $response = json_decode($this->response->getContent());
        $this->assertEquals($route->id, $response->id);
        $this->assertResponseOk();
        $this->assertCount(2, $response->records);
        foreach ($response->records as $item) {
            if ($item->sport_id == $sportOne->id) {
                $this->assertEquals($recordSportOne->duration_seconds, $item->duration_seconds);
            } else if ($item->sport_id == $sportTwo->id){
                $this->assertEquals($recordSportTwo->duration_seconds, $item->duration_seconds);
            }
        }
    }

    public function testPostRoutesMe()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $data = [
            'title' => "New title",
            'private_state' => App\Route::PRIVATE_STATE_FRIENDS,
            'distance' => 3.36,
            'center' => array('lat' => 43.243749, 'lon' => 76.896513),
            'route' => array(
                array('lat' => 43.24313166195505, 'lon' => 76.89881443977356),
                array('lat' => 43.2446556564029, 'lon' => 76.89858913421631),
                array('lat' => 43.24434304526531, 'lon' => 76.89423322677612),
                array('lat' => 43.24284248947451, 'lon' => 76.89440488815308),
                array('lat' => 43.243139477408434, 'lon' => 76.89873933792114)
            ),
        ];

        //Act
        $token = JWTAuth::fromUser($user);
        $this->call('POST', '/api/v1/routes/me', $data, [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $response = json_decode($this->response->getContent());
        $this->assertEquals($data['title'], $response->title);
        $this->assertNotNull($response->picture);
    }

    public function testPutRoute()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $data = [
            'title' => "Old title",
            'user_id' => $user->id,
            'private_state' => App\Route::PRIVATE_STATE_FRIENDS,
            'distance' => 3.36,
            'center' => array('lat' => 43.243749, 'lng' => 76.896513),
            'route' => json_encode(array(
                array('lat' => 43.24313166195505, 'lng' => 76.89881443977356),
                array('lat' => 43.2446556564029, 'lng' => 76.89858913421631),
                array('lat' => 43.24434304526531, 'lng' => 76.89423322677612),
                array('lat' => 43.24284248947451, 'lng' => 76.89440488815308),
                array('lat' => 43.243139477408434, 'lng' => 76.89873933792114)
            )),
        ];
        $routeOld = App\Route::create($data);

        //Act
        $token = JWTAuth::fromUser($user);
        $this->call('PUT', '/api/v1/routes/'.$routeOld->id, ['title' => 'New title'], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $response = json_decode($this->response->getContent());
        $this->assertEquals('New title', $response->title);
    }

    public function testDeleteRoute()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $data = [
            'title' => "Old title",
            'user_id' => $user->id,
            'private_state' => App\Route::PRIVATE_STATE_FRIENDS,
            'distance' => 3.36,
            'center' => array('lat' => 43.243749, 'lng' => 76.896513),
            'route' => json_encode(array(
                array('lat' => 43.24313166195505, 'lng' => 76.89881443977356),
                array('lat' => 43.2446556564029, 'lng' => 76.89858913421631),
                array('lat' => 43.24434304526531, 'lng' => 76.89423322677612),
                array('lat' => 43.24284248947451, 'lng' => 76.89440488815308),
                array('lat' => 43.243139477408434, 'lng' => 76.89873933792114)
            )),
        ];
        $routeOld = App\Route::create($data);

        //Act
        $token = JWTAuth::fromUser($user);
        $this->call('DELETE', '/api/v1/routes/'.$routeOld->id,[], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);
        $this->assertResponseOk();
    }

    public function testPostRecord()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $route = factory(App\Route::class)->make();
        $user->routes()->save($route);

        $sport = factory(App\Sport::class)->create();

        //Act
        $token = JWTAuth::fromUser($user);
        $this->call('POST', '/api/v1/routes/'.$route->id.'/record',
            ['sport_id'=>$sport->id, 'duration_seconds' => 3000], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $response = json_decode($this->response->getContent());
        $this->assertResponseOk();
        $this->assertObjectHasAttribute('place', $response);
    }

    public function testGetRecords()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $route = factory(App\Route::class)->make();
        $user->routes()->save($route);

        $sport = factory(App\Sport::class)->create();
        $record = Record::create([
            'user_id' => $user->id,
            'sport_id' => $sport->id,
            'route_id' => $route->id,
            'duration_seconds' => '3600']);

        $record = Record::create([
            'user_id' => $user->id,
            'sport_id' => $sport->id,
            'route_id' => $route->id,
            'duration_seconds' => '600']);

        //Act
        $token = JWTAuth::fromUser($user);
        $this->call('GET', '/api/v1/routes/'.$route->id.'/records/'.$sport->id,
            [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $response = json_decode($this->response->getContent())->data;
        $this->assertResponseOk();
        $this->assertCount(2, $response);
    }

}
