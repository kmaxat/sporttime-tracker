<?php

use App\Friend;
use App\GroupTraining;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InboxControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseTransactions;

    public function testGetAll()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();
        $friend = factory(App\User::class)->create();

        $friendRequest = factory(App\Friend::class)->make();
        $friendRequest->sender_id = $user->id;
        $friendRequest->receiver_id = $friend->id;
        $friendRequest->status = Friend::REQUESTED;
        $friendRequest->save();

        //Create my training
        $groupTraining = factory(App\GroupTraining::class)->make();
        $groupTraining->user_id = $user->id;
        $groupTraining->sport_id = $sport->id;
        $groupTraining->invites_user = array($friend->id);
        $groupTraining->save();

        GroupTraining::putMapping($ignoreConflicts = true);
        $groupTraining = GroupTraining::find($groupTraining->id);
        $groupTraining->addToIndex();

        $groupTraining->title = 'Tesf';
        $groupTraining->save();


        //Act
        $token = JWTAuth::fromUser($friend);
        $response = $this->call('GET', '/api/v1/inbox/me', [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        //Assert
        $data = json_decode($response->getContent())->data;
        $this->assertResponseOk();
        $this->assertCount(3,$data);


    }
}
