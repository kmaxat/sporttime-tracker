<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthenticateControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseTransactions;


    public function testRegisterUserThroughEmail()
    {
        Artisan::call('index:delete');
        Artisan::call('index:create');

        //Arrange
        $user = factory(App\User::class)->make();

        //Act
        $this->post('api/v1/auth/register', [
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'gender' => $user->gender,
            'birthday' => $user->birthday,
            'password' => $user->password]);

        //Assert
        $this->assertResponseOk();
        $this->seeJsonStructure( ['token']);
        $this->seeInDatabase('users', ['email' => $user->email]);

    }

    public function testLoginUserThroughEmail()
    {
        $user = factory(App\User::class)->create();
        $this->post('/api/v1/auth/login-email',  [
            'email' => $user->email,
            'password' => 'qweqwe'])
            ->seeJsonStructure( ['token']);
    }

    public function testRefreshToken(){
        $user = factory(App\User::class)->create();
        $token = JWTAuth::fromUser($user);
        $request = $this->get('/api/v1/auth/refresh-token',
            ['HTTP_Authorization' => 'Bearer ' . $token])
            ->seeJsonStructure( ['token']);
    }

    public function testSendEmailReset(){
        //Arrange
        $user = factory(App\User::class)->create();
        //Act & Assert
        $this->seeInDatabase('users', ['email' => $user->email]);
        $request = $this->post('api/v1/auth/password/email/', [
            'email' => $user->email])->seeJson(['message' => 'success']);

    }
}
