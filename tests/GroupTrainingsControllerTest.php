<?php

use App\GroupTraining;
use App\Invitation;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GroupTrainingsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseTransactions;

    public function testGetGroupTrainings()
    {
        Artisan::call('index:delete');
        Artisan::call('index:create');
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();
        $groupTraining = factory(App\GroupTraining::class)->make();
        $groupTraining->sport_id = $sport->id;
        $groupTraining->user_id = $user->id;
        $groupTraining->start_date = '2016-05-13';
        $groupTraining->save();

        GroupTraining::putMapping($ignoreConflicts = true);
        $groupTrainingES = GroupTraining::findOrFail($groupTraining->id);
        $groupTrainingES->addToIndex();
        sleep(1);

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('GET', '/api/v1/grouptrainings', ['lat' => '13','lon' => '30', 'date' => '2016-05-13'],
            [],[],['HTTP_Authorization' => 'Bearer ' . $token],[]);

        //Assert
        $data = json_decode($response->getContent())->data;
        $this->assertResponseOk();
        $this->assertCount(1, $data);
        $this->assertEquals($groupTrainingES->id, $data[0]->id);
    }

    public function testPostGroupTraining()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();
        $groupTraining = factory(App\GroupTraining::class)->make();
        $groupTraining->sport_id = $sport->id;

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('POST', '/api/v1/grouptrainings/me', $groupTraining->toArray(), [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        //Assert
        $this->assertResponseOk();
        $response = json_decode($this->response->getContent());
        $this->assertEquals($groupTraining->title, $response->title);

    }

    public function testGetGroupTrainingsMe()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();

        //Create my training
        $groupTraining = factory(App\GroupTraining::class)->make();
        $groupTraining->sport_id = $sport->id;
        $groupTraining->user_id = $user->id;
        $groupTraining->save();

        //Create users training, include and check if I'm there
        $trainingCreator = factory(App\User::class)->create();
        $createdGroupTraining = factory(App\GroupTraining::class)->make();
        $createdGroupTraining->sport_id = $sport->id;
        $createdGroupTraining->user_id = $trainingCreator->id;
        $createdGroupTraining->invites_user = array($user->id);
        $createdGroupTraining->save();

        GroupTraining::putMapping($ignoreConflicts = true);
        $createdGroupTraining = GroupTraining::find($createdGroupTraining->id);
        $createdGroupTraining->addToIndex();

        $invitation = Invitation::where('group_training_id', $createdGroupTraining->id)
        ->where('receiver_id', $user->id)->firstOrFail();
        $invitation->state = Invitation::STATE_ACCEPTED;
        $invitation->save();

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('GET', '/api/v1/grouptrainings/me', [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $groupTrainings = json_decode($response->getContent());
        $this->assertCount(2, $groupTrainings->data);

        if($groupTrainings->data[0]->id == $groupTraining->id)
            $this->assertEquals($groupTraining->description, $groupTrainings->data[0]->description);
        if ($groupTrainings->data[1]->id == $createdGroupTraining->id) {
            $this->assertEquals($createdGroupTraining->user_id, $groupTrainings->data[1]->user_id);
            $this->assertEquals('1', $groupTrainings->data[1]->count_accepted);
        }

    }

    public function testGetGroupTraining()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();
        $userMaybe = factory(App\User::class)->create();
        $userRejected = factory(App\User::class)->create();

        //Create users training, include and check if I'm there
        $trainingCreator = factory(App\User::class)->create();
        $createdGroupTraining = factory(App\GroupTraining::class)->make();
        $createdGroupTraining->user_id = $trainingCreator->id;
        $createdGroupTraining->sport_id = $sport->id;
        $createdGroupTraining->invites_user = array($user->id, $userMaybe->id, $userRejected->id);
        $createdGroupTraining->save();

        GroupTraining::putMapping($ignoreConflicts = true);
        $createdGroupTraining = GroupTraining::find($createdGroupTraining->id);
        $createdGroupTraining->addToIndex();


        $invitation = Invitation::where('group_training_id', $createdGroupTraining->id)
            ->where('receiver_id', $user->id)->firstOrFail();
        $invitation->state = Invitation::STATE_ACCEPTED;
        $invitation->save();

        $invitation = Invitation::where('group_training_id', $createdGroupTraining->id)
            ->where('receiver_id', $userMaybe->id)->firstOrFail();
        $invitation->state = Invitation::STATE_MAYBE;
        $invitation->save();

        $invitation = Invitation::where('group_training_id', $createdGroupTraining->id)
            ->where('receiver_id', $userRejected->id)->firstOrFail();
        $invitation->state = Invitation::STATE_REJECTED;
        $invitation->save();

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('GET', '/api/v1/grouptrainings/'.$createdGroupTraining->id, [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $groupTraining = json_decode($response->getContent());
        $this->assertEquals($createdGroupTraining->user_id, $groupTraining->user->id);
        $this->assertEquals('1', $groupTraining->count_accepted);
        $this->assertEquals('3', $groupTraining->count_invited);
        $this->assertEquals('1', $groupTraining->count_maybe);

    }

    public function testPutGroupTraining()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();

        //Create my training
        $groupTraining = factory(App\GroupTraining::class)->make();
        $groupTraining->user_id = $user->id;
        $groupTraining->sport_id = $sport->id;

        $groupTraining->save();
//        dd($groupTraining);

        GroupTraining::putMapping($ignoreConflicts = true);
        $groupTraining = GroupTraining::find($groupTraining->id);
        $groupTraining->addToIndex();

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('PUT', '/api/v1/grouptrainings/'.$groupTraining->id, ['description' => 'New desc'], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $groupTraining = json_decode($response->getContent());
        $this->assertResponseOk();
        $response = json_decode($this->response->getContent());
        $this->assertEquals('New desc', $response->description);

    }

    public function testDeleteGroupTraining()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();


        //Create my training
        $groupTraining = factory(App\GroupTraining::class)->make();
        $groupTraining->user_id = $user->id;
        $groupTraining->sport_id = $sport->id;
        $groupTraining->save();
        $this->seeInDatabase('group_trainings',['id'=>$groupTraining->id]);

        GroupTraining::putMapping($ignoreConflicts = true);
        $groupTraining = GroupTraining::find($groupTraining->id);
        $groupTraining->addToIndex();

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('DELETE', '/api/v1/grouptrainings/'.$groupTraining->id, [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);
        $this->assertResponseOk();
    }

    public function testPostGroupTrainingDecision()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();


        $trainingCreator = factory(App\User::class)->create();
        $createdGroupTraining = factory(App\GroupTraining::class)->make();
        $createdGroupTraining->user_id = $trainingCreator->id;
        $createdGroupTraining->sport_id = $sport->id;
        $createdGroupTraining->invites_user = array($user->id);
        $createdGroupTraining->save();

        GroupTraining::putMapping($ignoreConflicts = true);
        $createdGroupTraining = GroupTraining::find($createdGroupTraining->id);
        $createdGroupTraining->addToIndex();

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('POST', '/api/v1/grouptrainings/'.$createdGroupTraining->id.'/decision',
            ['decision'=>Invitation::STATE_ACCEPTED], [], [], ['HTTP_Authorization' => 'Bearer ' . $token],[]);
        $this->seeInDatabase('invitations',['group_training_id' => $createdGroupTraining->id,
            'state' => Invitation::STATE_ACCEPTED, 'receiver_id' => $user->id]);
        $groupTraining = json_decode($response->getContent());
        $this->assertResponseOk();
    }

    public function testGetGroupTrainingUserList()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();

        $userAccepted = factory(App\User::class)->create();
        $userMaybe = factory(App\User::class)->create();
        $userRejected = factory(App\User::class)->create();

        //Create users training, include and check if I'm there
        $trainingCreator = factory(App\User::class)->create();
        $createdGroupTraining = factory(App\GroupTraining::class)->make();
        $createdGroupTraining->user_id = $trainingCreator->id;
        $createdGroupTraining->sport_id = $sport->id;
        $createdGroupTraining->invites_user = array($user->id, $userAccepted->id, $userMaybe->id, $userRejected->id);
        $createdGroupTraining->save();

        GroupTraining::putMapping($ignoreConflicts = true);
        $createdGroupTraining = GroupTraining::find($createdGroupTraining->id);
        $createdGroupTraining->addToIndex();


        $invitation = Invitation::where('group_training_id', $createdGroupTraining->id)
            ->where('receiver_id', $userAccepted->id)->firstOrFail();
        $invitation->state = Invitation::STATE_ACCEPTED;
        $invitation->save();

        $invitation = Invitation::where('group_training_id', $createdGroupTraining->id)
            ->where('receiver_id', $userMaybe->id)->firstOrFail();
        $invitation->state = Invitation::STATE_MAYBE;
        $invitation->save();

        $invitation = Invitation::where('group_training_id', $createdGroupTraining->id)
            ->where('receiver_id', $userRejected->id)->firstOrFail();
        $invitation->state = Invitation::STATE_REJECTED;
        $invitation->save();

        //Act
        $token = JWTAuth::fromUser($user);
        $responseAccepted = $this->call('GET', '/api/v1/grouptrainings/'.$createdGroupTraining->id.'/'.Invitation::STATE_ACCEPTED, [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);
        $response = json_decode($responseAccepted->getContent());
        $this->assertCount(1, $response->data);

        $responseMaybe = $this->call('GET', '/api/v1/grouptrainings/'.$createdGroupTraining->id.'/'.Invitation::STATE_MAYBE, [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);
        $response = json_decode($responseMaybe->getContent());
        $this->assertCount(1, $response->data);

        $responseInvited = $this->call('GET', '/api/v1/grouptrainings/'.$createdGroupTraining->id.'/'.Invitation::STATE_INVITED, [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);
        $response = json_decode($responseInvited->getContent());
        $this->assertCount(2, $response->data);

    }
}
