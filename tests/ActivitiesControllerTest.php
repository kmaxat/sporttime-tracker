<?php

use App\Activity;
use App\Friend;
use App\Invitation;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivitiesControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseTransactions;

    public function testGetAll()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $invitedUser = factory(App\User::class)->create();
        $friend = factory(App\User::class)->create();

        $sport = factory(App\Sport::class)->create();
        $training = factory(App\Training::class)->create(['sport_id' => $sport->id,
            'user_id' => $friend->id]);
        $groupTraining = factory(App\GroupTraining::class)->create(['sport_id' => $sport->id,
            'user_id' => $user->id,
            'invites_user' => array($invitedUser->id, $friend->id)
        ]);

        $invitation = Invitation::where('sender_id', $user->id)
            ->where('receiver_id', $invitedUser->id)
            ->where('group_training_id', $groupTraining->id)
            ->firstOrFail();

        $invitation->state = Invitation::STATE_ACCEPTED;
        $invitation->save();

        $friendRequest = factory(App\Friend::class)->create([
            'sender_id' => $user->id,
            'receiver_id' => $friend->id,
            'status' => Friend::REQUESTED
        ]);
        $friendRequest->status = Friend::ACCEPTED;
        $friendRequest->save();

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('GET', '/api/v1/activities/me', [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $this->assertResponseOk();
        $activity = json_decode($response->getContent())->data;

        //Assert
        $this->seeInDatabase('activities',['user_id'=> $friend->id,
            'subject_id' => $training->id,
            'subject_action' => Activity::FINISHED_TRAINING]);

        $this->seeInDatabase('activities',['user_id'=> $user->id,
            'subject_id' => $groupTraining->id,
            'subject_action' => Activity::CREATED_GROUP_TRAINING]);

        $this->seeInDatabase('activities',['user_id'=> $invitedUser->id,
            'subject_id' => $groupTraining->id,
            'subject_action' => Activity::ACCEPTED_INVITATION]);

        $this->seeInDatabase('activities',['user_id'=> $user->id,
            'subject_id' => $friend->id,
            'subject_action' => Activity::BECAME_FRIENDS]);
    }

    public function testGetActivityByID()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $friend = factory(App\User::class)->create();

        $friendRequest = factory(App\Friend::class)->create([
            'sender_id' => $user->id,
            'receiver_id' => $friend->id,
            'status' => Friend::REQUESTED
        ]);
        $friendRequest->status = Friend::ACCEPTED;
        $friendRequest->save();

        //Act
        $token = JWTAuth::fromUser($user);
        $response = $this->call('GET', '/api/v1/activities/'.$friend->id, [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $activity = json_decode($response->getContent())->data;
        $this->assertResponseOk();

        //Assert
        $this->seeInDatabase('activities',['user_id'=> $user->id,
            'subject_id' => $friend->id,
            'subject_action' => Activity::BECAME_FRIENDS]);
    }

}
