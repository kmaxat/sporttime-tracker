<?php

use App\Setting;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SettingsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetSettings()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        Setting::create(['user_id' => $user->id]);

        //Act
        $token = JWTAuth::fromUser($user);
        $this->call('GET', '/api/v1/settings/me',
            [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $response = json_decode($this->response->getContent());
        $this->assertResponseOk();

    }

    public function testPutSettings()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        Setting::create(['user_id' => $user->id]);

        //Act
        $token = JWTAuth::fromUser($user);
        $this->call('PUT', '/api/v1/settings/me',
            ['invitation_notifications' => false], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);

        $response = json_decode($this->response->getContent());
        $this->assertResponseOk();
        $this->assertEquals(false, $response->invitation_notifications);

    }
}
