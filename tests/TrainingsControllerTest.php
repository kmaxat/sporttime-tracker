<?php

use App\Http\Transformers\TrainingsTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TrainingsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    use DatabaseTransactions;

    public function testGetTrainings()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $training = factory(App\Training::class)->make();
        $sport = factory(App\Sport::class)->create();
        $training->sport_id = $sport->id;
        $user->trainings()->save($training);

        //Act & Assert
        $token = JWTAuth::fromUser($user);

        $request = $this->get('api/v1/trainings/me',
            ['HTTP_Authorization' => 'Bearer ' . $token])
            ->seeJson( ['user_id' => $user->id,
                        'id' => $training->id]);

    }

    public function testGetTraining()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $training = factory(App\Training::class)->make();
        $sport = factory(App\Sport::class)->create();
        $training->sport_id = $sport->id;
        $user->trainings()->save($training);

        //Act & Assert
        $token = JWTAuth::fromUser($user);
        $request = $this->get('api/v1/trainings/'.$training->id,
            ['HTTP_Authorization' => 'Bearer ' . $token])
            ->seeJson( ['user_id' => $user->id,
                'id' => $training->id]);
    }

    public function testPostTraining()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();
        $training = factory(App\Training::class)->make();
        $training->sport_id = $sport->id;
        $training['notes'] = bcrypt('notes');
        $trainingArray = $training->toArray();

        //Act & Assert
        $token = JWTAuth::fromUser($user);
        $request = $this->post('api/v1/trainings/me',
            $trainingArray,
            ['HTTP_Authorization' => 'Bearer ' . $token])
            ->seeJson(['notes' => $trainingArray['notes']]);
        $this->seeInDatabase('trainings', ['notes' => $trainingArray['notes']]);
    }

    public function testDeleteTraining()
    {
        //Arrange
        $user = factory(App\User::class)->create();
        $sport = factory(App\Sport::class)->create();
        $training = factory(App\Training::class)->make();
        $training->sport_id = $sport->id;
        $user->trainings()->save($training);

        //Act & Assert
        $token = JWTAuth::fromUser($user);
        $response = $this->call('DELETE', 'api/v1/trainings/'.$training->id, [], [], [],
            ['HTTP_Authorization' => 'Bearer ' . $token],[]);
        $this->assertResponseOk();
    }


}
