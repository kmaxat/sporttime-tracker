<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Activity
 *
 * @property integer $id
 * @property integer $subject_id
 * @property string $subject_type
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Training $training
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereSubjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereSubjectType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Activity whereUpdatedAt($value)
 */
	class Activity extends \Eloquent {}
}

namespace App{
/**
 * App\GroupTraining
 *
 * @property integer $id
 * @property integer $sport_id
 * @property integer $user_id
 * @property string $location
 * @property string $title
 * @property string $description
 * @property float $price
 * @property string $private_state
 * @property string $start_date
 * @property string $start_time
 * @property string $end_time
 * @property string $repeat_type
 * @property string $repeat_days
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $invites_user
 * @property-read \App\Sport $sport
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereSportId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining wherePrivateState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereStartDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereStartTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereEndTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereRepeatType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereRepeatDays($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\GroupTraining whereInvitesUser($value)
 */
	class GroupTraining extends \Eloquent {}
}

namespace App{
/**
 * App\Invitation
 *
 * @property integer $id
 * @property integer $training_id
 * @property integer $sender_id
 * @property integer $receiver_id
 * @property string $state
 * @property \Carbon\Carbon $expires_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $sender
 * @property-read \App\User $receiver
 * @property-read \App\GroupTraining $groupTraining
 * @method static \Illuminate\Database\Query\Builder|\App\Invitation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invitation whereTrainingId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invitation whereSenderId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invitation whereReceiverId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invitation whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invitation whereExpiresAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invitation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invitation whereUpdatedAt($value)
 */
	class Invitation extends \Eloquent {}
}

namespace App{
/**
 * App\Social
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $social_name
 * @property string $social_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Social whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Social whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Social whereSocialName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Social whereSocialId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Social whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Social whereUpdatedAt($value)
 */
	class Social extends \Eloquent {}
}

namespace App{
/**
 * App\Sport
 *
 * @property integer $id
 * @property string $title
 * @property string $title_eng
 * @property string $filename
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Sport whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sport whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sport whereTitleEng($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sport whereFilename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sport whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sport whereUpdatedAt($value)
 */
	class Sport extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $role
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property float $weight
 * @property string $birthday
 * @property integer $height
 * @property string $picture
 * @property string $unit
 * @property string $onesignal_id
 * @property boolean $verified
 * @property string $token
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Training[] $trainings
 * @property-read \Elasticquent\ElasticquentCollection|\App\GroupTraining[] $groupTrainings
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRole($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereBirthday($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePicture($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUnit($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereOnesignalId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereVerified($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

