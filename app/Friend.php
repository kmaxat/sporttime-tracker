<?php

namespace App;

use App\Http\Transformers\FriendTransformer;
use Carbon\Carbon;
use NotificationService;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\Manager;
use League\Fractal;

use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use Redis;
use UserService;

class Friend extends Model
{
    protected $table = 'friends';

    protected $fillable = ['sender_id', 'receiver_id', 'status'];

    const ACCEPTED = 'accepted';
    const REJECTED = 'rejected';
    const REQUESTED = 'requested';
    const REMOVE = 'remove';
    public static $STATUS = [
        self::ACCEPTED,
        self::REJECTED,
        self::REQUESTED,
    ];
    //Used in NotificationsService, generating payload for InboxController
    const FRIEND_REQUEST = 'friend_request';

    public static function boot()
    {
        parent::boot();
        static::created(function ($friends){
            $fractal = new Manager();
            $fractal->setSerializer(new ArraySerializer());

            $user = UserService::findUserbyId($friends->sender_id);
            $userResource = new Item($user, new FriendTransformer());
            $userJson = $fractal->createData($userResource)->toJson();
            $date = Carbon::now();
            $payload = json_encode(array(
                Friend::FRIEND_REQUEST =>  ['user' => $userJson],
                'date'  => $date->toDateTimeString()
            ));
            Notification::create([
                'user_id' => $friends->receiver_id,
                'payload' => $payload
            ]);

            $notificationData = [
                'sender' => $user->first_name.' '.$user->last_name,
                'receiver_id' => $friends->receiver_id,
            ];
            NotificationService::queue(Friend::FRIEND_REQUEST, $notificationData);

        });
        static::updated(function ($friends) {
            if ($friends->status == Friend::ACCEPTED) {
                Activity::create([
                    'subject_id' => $friends->receiver_id,
                    'subject_type' => 'App\User',
                    'subject_action' =>  Activity::BECAME_FRIENDS,
                    'user_id' => $friends->sender_id,
                ]);
            }
        });
    }
}
