<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1',function($api){

    $api->group(['namespace' => 'App\Http\Controllers', 'prefix' => 'v1'], function($api) {
        $api->post('auth/register', 'AuthenticateController@register');
        $api->post('auth/login-email', 'AuthenticateController@login');
        $api->get('auth/login-facebook', 'AuthenticateController@login_facebook');
//        $api->get('auth/login-vk', 'AuthenticateController@login_vk');
        $api->get('auth/login-google', 'AuthenticateController@login_google');
        $api->post('auth/password/email', 'AuthenticateController@send_reset_link');

        $api->group(['middleware' => 'api.auth', 'providers' => ['jwt']], function ($api) {
            $api->get('auth/refresh-token', 'AuthenticateController@refreshToken');

            //Users
            $api->get('users/me', 'UsersController@getMe');
            $api->post('users/me', 'UsersController@putMe');
            $api->post('users/me/password', 'UsersController@postMePassword');
            $api->get('users/search', 'UsersController@getSearch');
            $api->get('users/{id}', 'UsersController@getUser');
            $api->get('users/me/email', 'UsersController@resendEmail');

            //Tracker
            $api->get('trainings/me', 'TrainingsController@getTrainings');
            $api->get('trainings/{id}', 'TrainingsController@getTraining');
            $api->post('trainings/me', 'TrainingsController@postTraining');
            $api->delete('trainings/{id}', 'TrainingsController@deleteTraining');

            //Group trainings
            $api->get('grouptrainings', 'GroupTrainingsController@getGroupTrainings');
            $api->get('grouptrainings/me', 'GroupTrainingsController@getGroupTrainingsMe');
            $api->get('grouptrainings/{id}', 'GroupTrainingsController@getGroupTraining');

            $api->get('grouptrainings/{id}/{type}', 'GroupTrainingsController@getGroupTrainingUserList');

            $api->put('grouptrainings/{id}', 'GroupTrainingsController@putGroupTraining');
            $api->post('grouptrainings/me', 'GroupTrainingsController@postGroupTraining');
            $api->delete('grouptrainings/{id}', 'GroupTrainingsController@deleteGroupTraining');
            $api->post('grouptrainings/{id}/decision', 'GroupTrainingsController@postGroupTrainingDecision');

            //Friends
            $api->get('friends/me', 'FriendsController@getMeFriends');
            $api->post('friends/{id}', 'FriendsController@postFriendshipRequest');

            //Activity Feed
            $api->get('activities/me', 'ActivitiesController@getAll');
            $api->get('activities/{id}', 'ActivitiesController@getAll');

            //Inbox
            $api->get('inbox/me', 'InboxController@getAll');
            $api->delete('inbox/{id}', 'InboxController@deleteNotification');

            //Sport
            $api->get('sports', 'SportsController@getAll');

            //Routes
            $api->get('routes', 'RoutesController@getRoutes');
            $api->get('routes/me', 'RoutesController@getRoutesMe');
            $api->post('routes/me', 'RoutesController@postRoutesMe');
            $api->put('routes/{id}', 'RoutesController@putRoute');
            $api->get('routes/{id}', 'RoutesController@getRoute');
            $api->delete('routes/{id}', 'RoutesController@deleteRoute');
            $api->post('routes/{id}/record', 'RoutesController@postRecord');
            $api->get('routes/{id}/records', 'RoutesController@getRecords');

            //Settings
            $api->get('settings/me', 'SettingsController@getSettings');
            $api->put('settings/me', 'SettingsController@putSettings');

        });

        //Routes for email confirmation and email resets
        $api->group(['middleware' => 'web'], function ($api){
            $api->get('auth/register/confirm/{token}', 'AuthenticateController@confirm_email');
            $api->get('auth/password/reset/{token?}', 'AuthenticateController@showResetForm');
            $api->post('auth/password/reset', 'AuthenticateController@reset');
        });
    });
});

Route::resource('users', 'admin\AdminUsersController');
Route::resource('grouptrainings', 'admin\AdminGroupTrainingsController');
Route::resource('sports', 'admin\AdminSportsController');
Route::resource('routes', 'admin\AdminRoutesController');
Route::get('home', 'HomeController@index');

Route::get('/', 'PagesController@index')->name('home.index');
Route::get('/faq', 'PagesController@faq')->name('home.faq');
Route::get('/about', 'PagesController@about')->name('home.about');

Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);
Route::post('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

// Registration Routes...
Route::get('register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@showRegistrationForm']);
Route::post('register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@register']);

// Password Reset Routes...
Route::get('password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
Route::post('password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
Route::post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);
