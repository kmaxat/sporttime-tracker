<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTrainingRequest;
use App\Http\Requests;
use App\Services\TrainingService;
use App\Sport;
use App\Training;
use App\User;
use Carbon\Carbon;
use DB;
use Dingo\Api\Routing\Helpers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Transformers\TrainingsTransformer;
use Illuminate\Support\Facades\Log;
use League\Fractal\Serializer\ArraySerializer;


class TrainingsController extends Controller
{
    use Helpers;

    public function getTrainings(Request $request)
    {
        $user = $this->user();

        $query = Training::where('user_id', $user->id)->orderBy('created_at', 'desc')->with('sport');

        if ($request->has('sport')) {
            $sport = Sport::where('id',$request->input('sport'))->first();
            if ($sport === null) {
                abort(400, 'Failed to find sport with specified id');
            } else
                $query->where('sport_id', $sport->id)->get();
        }

        if($request->has('date')){
            $query->whereRaw('date(created_at) = ?', [$request->date]);
        }

        $trainings = $query->get();
        return $this->response->collection($trainings, new TrainingsTransformer());
    }

    public function getTraining($id)
    {
        try {
            $training = Training::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(400, 'No training found with specified id');
        }
//        return $trainings;
        return $this->response->item($training, new TrainingsTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });

    }

    public function postTraining(CreateTrainingRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = $this->user()->id;
        $data['route'] = json_encode($request->get('route'));
        $training = Training::create($data);
        return $this->response->item($training, new TrainingsTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }


    public function deleteTraining($id)
    {
        $training = $this->user()->trainings->find($id);
        if(is_null($training)){
            abort(400, 'No training found with specified id');
        }
        $training->delete();
        return response()->json(['message' => 'success', 'status_code' => 200]);
    }
}
