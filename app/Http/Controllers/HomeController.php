<?php

namespace App\Http\Controllers;

use App\GroupTraining;
use App\Http\Requests;
use App\Sport;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::count();
        $sports = Sport::count();
        $grouptrainings = GroupTraining::count();
        return view('home')
            ->with('users', $users)
            ->with('sports',$sports)
            ->with('grouptrainings',$grouptrainings);
    }
}
