<?php

namespace App\Http\Controllers;
use App\GroupTraining;
use App\Http\Requests\CreateGroupTrainingRequest;
use App\Http\Requests\GetTrainingsRequest;
use App\Http\Requests\PostDecisionRequest;
use App\Http\Requests\UpdateGroupTrainingRequest;
use App\Http\Transformers\FriendsMeTransformer;
use App\Http\Transformers\FriendTransformer;
use App\Http\Transformers\GetGroupTrainingsTransformerMe;
use App\Http\Transformers\GroupTrainingsTransformer;
use App\Http\Transformers\PostGroupTrainingsTransformer;
use App\Invitation;
use App\User;
use Dingo\Api\Contract\Http\Request;
use Dingo\Api\Routing\Helpers;
use GroupTrainingService;
use InvitationService;
use App\Http\Requests;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\Fractal\Serializer\ArraySerializer;

class GroupTrainingsController extends Controller
{
    //
    use Helpers;

    public function getGroupTrainings(GetTrainingsRequest $request)
    {
        $limit = setLimit($request->input('limit'));
        try {
            $groupTraining  = GroupTrainingService::getTrainings($request->all(), $limit);
        } catch (\Exception $e) {
            abort(500, 'No mapping for group trainings are present');
        }
        return $this->response->paginator($groupTraining, new GetGroupTrainingsTransformerMe());
    }

    public function postGroupTraining(CreateGroupTrainingRequest $request)
    {
        $user = $this->user();
        $groupTraining = GroupTrainingService::create($user, $request->all());
        return $this->response->item($groupTraining, new PostGroupTrainingsTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function getGroupTrainingsMe(Request $request)
    {
        $limit = setLimit($request->input('limit'));

        $groupTrainings = GroupTrainingService::getUserTrainings($this->user()->id, $limit);
        return $this->response->paginator($groupTrainings, new GetGroupTrainingsTransformerMe());
    }

    public function getGroupTraining($id)
    {
        try {
            $groupTraining = GroupTraining::findOrFail($id)->load('user','sport');
            $groupCountsAccepted = InvitationService::getInvitationCountsByTrainingId(Invitation::STATE_ACCEPTED, $id);
            $groupCountsMaybe = InvitationService::getInvitationCountsByTrainingId(Invitation::STATE_MAYBE, $id);
            GroupTrainingService::setCountsAttribute($groupTraining,'count_accepted', $groupCountsAccepted);
            GroupTrainingService::setCountsAttribute($groupTraining, 'count_maybe', $groupCountsMaybe);
            $groupTraining->setAttribute('count_invited', sizeof($groupTraining->invites_user));

        } catch (ModelNotFoundException $e) {
            abort(400, 'No group training found with specified id');
        }
        return $this->response->item($groupTraining, new GroupTrainingsTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function getGroupTrainingUserList(Request $request, $id, $type)
    {
        $limit = setLimit($request->get('limit'));
        try {
            $groupTraining = GroupTraining::findOrFail($id);
        }
        catch (ModelNotFoundException $e) {
                abort(400, 'No group training found with specified id');
        }

        if ($type == Invitation::STATE_INVITED) {
            $invitations = Invitation::where('group_training_id', $groupTraining->id)
                ->where('state', Invitation::STATE_INVITED)
                ->orWhere('state', Invitation::STATE_REJECTED)
                ->select('receiver_id')->get();

        } elseif ($type == Invitation::STATE_ACCEPTED) {
            $invitations = Invitation::where('group_training_id', $groupTraining->id)
                ->where('state', Invitation::STATE_ACCEPTED)
                ->select('receiver_id')->get();

        } elseif ($type == Invitation::STATE_MAYBE) {
            $invitations = Invitation::where('group_training_id', $groupTraining->id)
                ->where('state', Invitation::STATE_MAYBE)
                ->select('receiver_id')->get();
        } else
            abort(400, 'Failed to find group trainings with specified type');
        $users = User::whereIn('id', $invitations)->paginate($limit);
        return $this->response->paginator($users, new FriendsMeTransformer());
    }


    public function putGroupTraining($id, UpdateGroupTrainingRequest $request)
    {
        $groupTraining = GroupTrainingService::update((int)$id, $request->all());
        return $this->response->item($groupTraining, new PostGroupTrainingsTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function deleteGroupTraining($id)
    {
        GroupTrainingService::delete((int)$id);
        return response()->json(['message' => 'success', 'status_code' => 200]);
    }

    public function postGroupTrainingDecision(PostDecisionRequest $request, $id)
    {
        $groupTraining = GroupTraining::findOrFail($id);
        try {
            InvitationService::decision($this->user()->id, $id, $request->decision);
        } catch (ModelNotFoundException $e) {
            if($groupTraining->private_state == GroupTraining::PRIVATE_STATE_CLOSE)
                abort('400', 'Failed to find group training with specified id');
            else {
                $toSave = [
                    'sender_id' => $groupTraining->user_id,
                    'state' => Invitation::STATE_ACCEPTED,
                    'receiver_id' => $this->user()->id,
                    'group_training_id' => $groupTraining->id,
                ];
                Invitation::create($toSave);
            }

        }

        return response()->json(['message' => 'success', 'status_code' => 200]);
    }
}
