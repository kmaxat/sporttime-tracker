<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Route;
use App\Sport;
use Auth;
use Dingo\Api\Routing\Helpers;
use File;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Log;

class AdminRoutesController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $routes= Route::orderBy('created_at','desc')->get();

        if ($request->has('message')) {
            $message = Input::get('message');
            return view('routes.index')->with('routes', $routes)->with('message',$message);
        }
        else
            return view('routes.index')->with('routes', $routes);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('routes.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log:info($request->all());
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'distance' => 'required|max:255',
            'route' => 'required|json',
            'picture' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
               'message' => [$validator->errors()->all()]],
                500);
        }

        $picture = $request->get('picture');
        $path = public_path('images/routes');
        if (!File::exists($path)) {
            File::makeDirectory($path, 493, true, false);
        }
        $img = Image::make($picture);
        $filename = uniqid('img_').'.jpg';
        $img->save($path.'/'.$filename);

        $routes = str_replace("lng", "lon", $request->get('route'));
        Route::create([
            'user_id' => Auth::id(),
            'title' => $request->get('title'),
            'route' => $routes,
            'private_state' => Route::PRIVATE_STATE_FRIENDS,
            'distance' => $request->get('distance'),
            'picture' => 'images/routes/'.$filename,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $route = Route::findOrFail($id);
        $route->route = str_replace("lon", "lng", $route->route);
        $route = json_decode($route);
        return view('routes.edit')->with('route',$route);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $route = Route::findOrFail($id);
        $attributes = $request->all();
        $route->fill($attributes);
        $route->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $route = Route::findOrFail($id);
        $route->delete();
        return redirect('/routes');
    }
}
