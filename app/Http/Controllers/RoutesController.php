<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRecordRequest;
use App\Http\Requests\PostRouteRequest;
use App\Http\Requests\PutRouteRequest;
use App\Http\Transformers\PostRouteTransformer;
use App\Http\Transformers\RecordsTransformer;
use App\Http\Transformers\RecordTransformer;
use App\Http\Transformers\RouteTransformer;
use App\Record;
use App\Route;
use App\Sport;
use App\User;
use DB;
use Dingo\Api\Routing\Helpers;
use File;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Image;
use League\Fractal\Serializer\ArraySerializer;

class RoutesController extends Controller
{
    //
    use Helpers;

    public function getRoutes(Request $request)
    {
        $limit = setLimit($request->get('limit'));
        $routes = Route::where('private_state', Route::PRIVATE_STATE_FRIENDS)->paginate($limit);
        return $this->response->paginator($routes, new PostRouteTransformer());
    }

    public function getRoutesMe(Request $request)
    {
        $limit = setLimit($request->get('limit'));
        $routes = $this->user()->routes()->paginate($limit);
        return $this->response->paginator($routes, new PostRouteTransformer());
    }

    public function getRoute($id)
    {
        try {
            $route = Route::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(400, 'No route found with specified id');
        }
        $records = Record::where('route_id', $route->id)
            ->select(DB::raw('sport_id, MIN(duration_seconds) as duration_seconds'))
            ->groupBy('sport_id')
            ->with('sport')
            ->get();

        $route->setAttribute('records', $records);

        return $this->response->item($route, new RouteTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function postRoutesMe(PostRouteRequest $request)
    {
        $data = $request->all();
        $path = "path=color:0x0000ff|weight:3";
        foreach ($data['route'] as $geopoint) {
            $path.="|".$geopoint['lat'].','.$geopoint['lon'];
        }
        $picture = "https://maps.googleapis.com/maps/api/staticmap?".
        "center=".$data['center']['lat'].','.$data['center']['lon'].
        "&size=1024x1024&format=jpg&maptype=roadmap&key=AIzaSyD3PUzj-kqVfJAZim2l7pCfHovOsREa-EA&".$path;

        if(strlen($picture) > 8192)
            $picture = substr($picture,0,8192);


        $path = public_path('images/routes');
        if (!File::exists($path)) {
            File::makeDirectory($path, 493, true, false);
        }
        $img = Image::make($picture);
        $filename = uniqid('img_').'.jpg';
        $img->save($path.'/'.$filename);

        $data['user_id'] = $this->user()->id;
        $data['picture'] = 'images/routes/' . $filename;
        $data['route'] = json_encode($data['route']);
        $route = Route::create($data);

        return $this->response->item($route, new PostRouteTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function putRoute(PutRouteRequest $request, $id)
    {
        try {
            $route = $this->user()->routes()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(400, 'No group training found with specified id');
        }
        $route->fill($request->all());
        $route->save();
        return $this->response->item($route, new PostRouteTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function deleteRoute($id)
    {
        try {
            $route = $this->user()->routes()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(400, 'No route found with specified id');
        }
        $route->delete();
        return response()->json(['message' => 'success', 'status_code' => 200]);
    }

    public function postRecord(PostRecordRequest $request, $id)
    {
        $userId = $this->user()->id;
        try {
            Route::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(400, 'Failed to find route with specified id');
        }

        $data = $request->all();
        $data['user_id'] = $userId;
        $data['route_id'] = $id;
        $record = Record::create($data);

        $records = Record::where('route_id', $id)
            ->orderBy('duration_seconds','asc')
            ->where('sport_id', $data['sport_id'])
            ->groupBy('duration_seconds')
            ->where('duration_seconds','<=',$data['duration_seconds'])
            ->get();
        $place = collect($records)->count();
        $record->setAttribute('place', $place);

        return $this->response->item($record, new RecordTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function getRecords(Request $request, $id)
    {
        $limit = setLimit($request->get('limit'));

        try {
            Route::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(400, 'Failed to find route with specified id');
        }

        $records = Record::where('route_id', $id)
            ->orderBy('duration_seconds', 'asc')
            ->with('user')
            ->with('sport')
            ->paginate($limit);
        return $this->response->paginator($records, new RecordsTransformer());
    }





}
