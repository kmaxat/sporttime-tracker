<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostSettingsRequest;
use App\Http\Transformers\SettingsTransformer;
use App\Setting;
use App\User;
use Dingo\Api\Routing\Helpers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use League\Fractal\Serializer\ArraySerializer;

class SettingsController extends Controller
{
    use Helpers;

    public function getSettings()
    {
        try {
            $user = User::where('id',$this->user()->id)->first();
            if ($user) {
                $settings = Setting::where('user_id', $this->user()->id)->first();
                if (!$settings) {
                    $settings = Setting::create([
                        'user_id' => $this->user()->id,
                        'invitation_notifications' => true,
                        'friend_request_notifications' => true
                    ]);
                }
            }

        } catch (ModelNotFoundException $e) {
            abort(500, 'Failed to find settings with specified user');
        }
        return $this->response->item($settings, new SettingsTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function putSettings(PostSettingsRequest $request)
    {
        $data = $request->all();
        $settings = Setting::where('user_id', $this->user()->id)->firstOrFail();
        $settings->fill($data);
        $settings->save();
        return $this->response->item($settings, new SettingsTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

}
