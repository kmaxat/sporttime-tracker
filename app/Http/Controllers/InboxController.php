<?php

namespace App\Http\Controllers;

use App\Friend;
use App\GroupTraining;
use App\Http\Transformers\FriendTransformer;
use App\Http\Transformers\GroupTrainingsTransformer;
use App\Notification;
use Dingo\Api\Contract\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\Fractal\Resource\Item;
use Dingo\Api\Routing\Helpers;

use App\Http\Requests;
use League\Fractal\Manager;
use League\Fractal;
use League\Fractal\Serializer\ArraySerializer;
use Redis;
use UserService;

class InboxController extends Controller
{
    //
    use Helpers;

    public function getAll(Request $request)
    {
        $userId = $this->user()->id;
        $notifications = Notification::where('user_id', $userId)->get();
        $json = [];
        $fractal = new Manager();
        $fractal->setSerializer(new ArraySerializer());

        foreach ($notifications as $notification) {
            $object = json_decode($notification->payload,true);

            if (array_key_exists(Friend::FRIEND_REQUEST, $object)) {
                $json[] = array(
                    'id'   => $notification->id,
                    'type' => Friend::FRIEND_REQUEST,
                    'date' => $object['date'],
                    'user' => json_decode($object['friend_request']['user'])
                );

            } else if(array_key_exists(GroupTraining::UPDATE, $object)) {
                $json[] = array(
                    'id'   => $notification->id,
                    'type' => GroupTraining::UPDATE,
                    'date' => $object['date'],
                    'updated_field' => $object['group_training_update']['updated_field'],
                    'group_training' => json_decode($object['group_training_update']['group_training'])
                );
            } else if (array_key_exists(GroupTraining::INVITATION, $object)) {
                $json[] = array(
                    'id'   => $notification->id,
                    'type'=> GroupTraining::INVITATION,
                    'date' => $object['date'],
                    'group_training' => json_decode($object['group_training_invitation']['group_training'])
                );
            }
        }
        return array("data"=>$json);
    }

    public function deleteNotification($id)
    {
        try {
            $notification = $this->user()->notifications()->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(400, 'No notification found with specified id');
        }
        $notification->delete();
        return response()->json(['message' => 'success', 'status_code' => 200]);
    }
}
