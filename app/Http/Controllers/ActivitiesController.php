<?php

namespace App\Http\Controllers;

use App\Activity;
use Log;
use UserService;
use App\Http\Transformers\ActivitiesTransformer;
use FriendService;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;

class ActivitiesController extends Controller
{
    use Helpers;
   
    public function getAll(Request $request, $id = null)
    {
        $limit = setLimit($request->input('limit'));
        if (!isset($id)) {
            $userId = $this->user()->id;
        } else {
            $userId = $id;
            UserService::findUserById($userId);
            FriendService::checkIfFriends($this->user()->id, $userId);
        }

        $friends = FriendService::getFriends($userId);
        $friendsIDs = FriendService::getFriendsIds($friends, $userId);
        //Exclude person's activites.
//        $friendsIDs[] = $userId;

        $activities = Activity::whereIn('user_id', $friendsIDs)
            ->orderBy('created_at', 'desc')
            ->with('user','subject')
            ->paginate($limit);
        return $this->response->paginator($activities, new ActivitiesTransformer());

    }
}
