<?php

namespace App\Http\Controllers;

use App\Friend;
use App\Http\Requests\PostFriendDecisionRequest;
use App\Http\Transformers\FriendsMeTransformer;
use App\Http\Transformers\FriendTransformer;
use App\User;
use FriendService;
use UserService;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\Http\Requests;

class FriendsController extends Controller
{
    use Helpers;

    public function getMeFriends(Request $request)
    {
        $limit = setLimit($request->input('limit'));
        $userId = $this->user()->id;
        $request = FriendService::getFriends($userId);
        $friendsIDs = FriendService::getFriendsIds($request, $userId);
        $friends = User::whereIn('id', $friendsIDs)
                ->orderBy('first_name', 'asc')
                ->paginate($limit);
        return $this->response->paginator($friends, new FriendsMeTransformer());

    }

    public function postFriendshipRequest(PostFriendDecisionRequest $request, $id)
    {
        $userId = $this->user()->id;
        UserService::findUserById($id);


        if ($userId == $id) {
            abort(400, 'User can not send friend request to herself/himself');
        }
        $friendRequest = FriendService::findExistingRequests($userId, $id);
        if (!isset($friendRequest)) {
            Friend::create([
                'sender_id' => $this->user()->id,
                'receiver_id' => $id,
                'status' => Friend::REQUESTED,
            ]);
        } else {
            if ($request->decision == Friend::REMOVE)
                Friend::destroy($friendRequest->id);
            if ($friendRequest->receiver_id == $userId) {
                if ($request->decision == Friend::ACCEPTED || $request->decision == Friend::REJECTED) {
                    $friendRequest->status = $request->decision;
                    $friendRequest->save();
                }
            }
        }
        return response()->json(['message' => 'success', 'status_code' => 200]);

    }
}
