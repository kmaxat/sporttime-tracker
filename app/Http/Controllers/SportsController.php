<?php

namespace App\Http\Controllers;

use App\Http\Transformers\SportTransformer;
use App\Sport;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\Serializer\DataArraySerializer;

class SportsController extends Controller
{
    //
    use Helpers;
    public function getAll()
    {
        $sports = Sport::orderBy('title','asc')->where('title_eng','!=','Other')->get();
        $otherSport = Sport::where('title_eng', 'Other')->first();
        $sports->push($otherSport);

        return $this->response->collection($sports, new SportTransformer);
    }
}
