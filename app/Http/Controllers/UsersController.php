<?php

namespace App\Http\Controllers;
use App\Friend;
use App\Http\Transformers\FriendTransformer;
use App\Http\Transformers\UserByIdTransformer;
use App\Http\Transformers\UserTransformer;
use FriendService;
use App\Training;
use App\User;
use DB;
use Dingo\Api\Contract\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateRequest;
use File;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\Serializer\JsonApiSerializer;
use UserService;

use App\Http\Requests;

class UsersController extends Controller
{
    //
    use Helpers;

    public function getMe()
    {
        return $this->response->item($this->user(), new UserTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function putMe(UpdateRequest $request)
    {
        $user   = UserService::update($this->user()->id, $request);
        return $this->response->item($user, new UserTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function postMePassword(UpdatePasswordRequest $request)
    {
        UserService::changePassword($this->user(), $request->get('password_old'), $request->get('password_new'));
        return response()->json(['message' => 'success', 'status_code' => 200]);
    }

    public function getUser($id)
    {
        //get user
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(400, 'Failed to find user with specified id');
        }

        $userId = $this->user()->id;
        $friends = Friend::where(function ($query) use ($userId,$id) {
            $query->where('sender_id',$userId)
                ->where('receiver_id',$id);
            })
            ->orWhere(function ($query) use ($userId,$id) {
                $query->where('sender_id', $id)
                    ->where('receiver_id', $userId);
            })
            ->first();
        if (count($friends)) {
            if($friends->status == Friend::ACCEPTED)
                $user->setAttribute('friends', 'accepted');
            else
                $user->setAttribute('friends', 'requested');
        }
        $trainings = Training::where('user_id', $id)->get();
        if (count($trainings)) {
            $user->setAttribute('trainings_count', count($trainings));
        }
        $friendCount = FriendService::getFriends($id);
        if (count($friendCount)) {
            $user->setAttribute('friends_count', count($friendCount));
        }


        return $this->response->item($user, new UserByIdTransformer, [], function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        });
    }

    public function getSearch(Request $request)
    {
        $query  = $request->input('q');
        $limit = setLimit($request->input('limit'));
        $users = UserService::search($query, $limit);
        return $this->response->paginator($users, new FriendTransformer());
   }

    public function resendEmail()
    {
        //TODO: Check if failed or queued
        UserService::sendConfirmationEmail($this->user());
        return response()->json(['message' => 'success', 'status_code' => 200]);
    }

}
