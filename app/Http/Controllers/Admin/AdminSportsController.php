<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Transformers\SportTransformer;
use App\Sport;
use Dingo\Api\Routing\Helpers;
use File;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;

class AdminSportsController extends Controller
{
    use Helpers;
    /**
            * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $sports= Sport::all();
        return view('sports.index')->with('sports', $sports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sports.create');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'title_eng' => 'required|max:255',
            'picture' => 'required|image|max:1024',
            'calories' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return redirect('/sports/create')
                ->withInput()
                ->withErrors($validator);
        }
        $picture = Input::file('picture');
        $path = public_path('images/sports');
        if (!File::exists($path)) {
            File::makeDirectory($path, 493, true, false);
        }
        $filename = uniqid('img_').'.'.$picture->getClientOriginalExtension();
        $picture->move($path, $filename);

        Sport::create([
            'title' => $request->get('title'),
            'title_eng' => $request->get('title_eng'),
            'calories' => $request->get('calories'),
            'picture' => 'images/sports/'.$filename,

        ]);
        return redirect('/sports')->with('message',$request->title.' добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $sport = Sport::findOrFail($id);
        return view('sports.edit')->with('sport',$sport);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $sport = Sport::findOrFail($id);
        $attributes = $request->all();
        if ($request->hasFile('picture')) {
            if (File::exists(public_path($sport->picture))) {
                File::delete(public_path($sport->picture));
            }
            $picture = Input::file('picture');
            $path = public_path('images/sports');
            if (!File::exists($path)) {
                File::makeDirectory($path, 493, true, false);
            }
            $filename = uniqid('img_').'.'.$picture->getClientOriginalExtension();
            $picture->move($path, $filename);
            $attributes = [
                'title' => $request->get('title'),
                'title_eng' => $request->get('title_eng'),
                'calories' => $request->get('calories'),
                'picture' => 'images/sports/'.$filename ];
        }
        $sport->fill($attributes);
        $sport->save();
        return redirect('/sports');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sport = Sport::findOrFail($id);
        $sport->delete();
        return redirect('/sports');
    }
}
