<?php

namespace App\Http\Controllers;
use App\Http\Requests\LoginSocialRequest;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;

use App\Http\Requests\UserIDRequest;
use App\Services\Social\Google;
use App\Services\Social\Vk;
use App\Services\Social\Facebook;
use App\User;
use Artisan;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Log;
use UserService;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;

use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthenticateController extends Controller
{
    use ResetsPasswords;

    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $data['role'] = User::ROLE_USER;
        DB::transaction(function () use ($data, &$token) {
            $user = UserService::createUser($data);
            try {
                $token = JWTAuth::fromUser($user);
            } catch (JWTException $e) {
                abort(500, 'Failed to create token');
            }
            UserService::sendConfirmationEmail($user);
        });
        return compact('token');
    }

    public function login(LoginRequest $request)
    {
        $user = Input::only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($user)) {
                abort(400, 'Email or password are incorrect');
            }
        } catch (JWTException $e) {
            abort(500, 'Failed to create token');
        }
        return compact('token');
    }


    public function refreshToken()
    {
        $token = JWTAuth::getToken();
        if (!$token) {
            abort(400, 'Token not provided');
        }
        try {
            $token = JWTAuth::refresh($token);
        } catch (TokenInvalidException $e) {
            abort(401, 'Token is invalid');

        }
        return compact('token');
    }

    public function login_facebook(LoginSocialRequest $request, Facebook $facebook)
    {
        $socialToken = $request->get('token');
        $userData = $facebook->getFacebookUserData($socialToken);
        DB::transaction(function () use ($userData, &$token) {
            $user = UserService::createUserBySocial('facebook', $userData);
            try {
                $token = JWTAuth::fromUser($user);
            } catch (JWTException $e) {
                abort(500, 'Failed to create token');
            }
        });
        return compact('token');
    }

    public function login_vk(UserIDRequest $request, VK $vk)
    {
        $user_id = $request->get('user_id');
        $userData = $vk->getVKUserData($user_id);
        DB::transaction(function () use ($userData, &$token) {
            $user = UserService::createUserBySocial('vk', $userData);
            try {
                $token = JWTAuth::fromUser($user);
            } catch (JWTException $e) {
                abort(500, 'Failed to create token');
            }
        });
        return compact('token');
    }

    public function login_google(LoginSocialRequest $request, Google $google)
    {
        $socialToken = $request->get('token');
        $userData = $google->getGoogleUserData($socialToken);
        DB::transaction(function () use ($userData, &$token) {
            $user = UserService::createUserBySocial('google', $userData);
            try {
                $token = JWTAuth::fromUser($user);
            } catch (JWTException $e) {
                abort(500, 'Failed to create token');
            }
        });
        return compact('token');
    }

    public function confirm_email($token)
    {
        try {
            User::whereToken($token)->firstOrFail()->confirmEmail();
        } catch (ModelNotFoundException $e) {
            abort(400, 'Failed to find specified token');
        }
        return view('message')->with('message_title','Email confirmation successful!');

    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function send_reset_link(PasswordResetRequest $request)
    {
        $broker = $this->getBroker();
        $response = Password::broker($broker)->sendResetLink($request->only('email'));
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return response()->json(['message' => 'success', 'status_code' => 200]);
            case Password::INVALID_USER:
            default:
                abort('500', 'Failed to send email');
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->getEmail();
        }

        $email = $request->input('email');
        $api = $request->input('api');
        if(isset($api))
            $url = url(env('API_PREFIX').'/'.env('API_VERSION').'/auth/password/reset');
        else
            $url = url('/password/reset');

        if (property_exists($this, 'resetView')) {
            return view($this->resetView)->with(compact('token', 'email','url'));
        }

        if (view()->exists('auth.passwords.reset')) {
            return view('auth.passwords.reset')->with(compact('token', 'email','url'));
        }

        return view('auth.reset')->with(compact('token', 'email','url'));
    }


    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        $this->validate(
            $request,
            $this->getResetValidationRules(),
            $this->getResetValidationMessages(),
            $this->getResetValidationCustomAttributes()
        );

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return view('message')->with('message_title','Password reset successful!');
            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }
}

