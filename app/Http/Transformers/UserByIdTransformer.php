<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 6/22/16
 * Time: 10:01 PM
 */

namespace App\Http\Transformers;


use App\User;
use League\Fractal\TransformerAbstract;

class UserByIdTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(User $user)
    {
        if (!empty($user->picture->picture)) {
            $picture = asset($user->picture);
        }
        else
            $picture = null;

        return [
            'id' => (int)$user['id'],
            'email' => $user['email'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'gender' => $user['gender'],
            'birthday' => $user['birthday'],
            'height' => (int)$user['height'],
            'weight' => (float)$user['weight'],
            'picture' => $picture,
            'friend' => $user->friends,
            'trainings_count' => (int) $user->trainings_count,
            'friends_count' => (int) $user->friends_count,
        ];
    }
}