<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/28/16
 * Time: 00:52
 */

namespace App\Http\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(User $user)
    {
        if (!empty($user->picture)) {
            $picture = asset($user->picture);
        }
        else
            $picture = null;
        return [
            'id'            => (int) $user['id'],
            'email'         => $user['email'],
            'first_name'    => $user['first_name'],
            'last_name'     => $user['last_name'],
            'gender'        => $user['gender'],
            'birthday'      => $user['birthday'],
            'unit'          => $user['unit'],
            'height'        => (int) $user['height'],
            'weight'        => (float) $user['weight'],
            'picture'       => $picture,
            'onesignal_id'  => $user['onesignal_id'],
            'verified'      => (bool) $user->verified,
        ];
    }
}