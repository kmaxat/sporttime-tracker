<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 6/3/16
 * Time: 00:11
 */

namespace App\Http\Transformers;


use App\Sport;
use League\Fractal\TransformerAbstract;

class SportTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(Sport $sport)
    {
        return [
            'id' => (int) $sport->id,
            'title' => $sport->title,
            'title_eng' => $sport->title_eng,
            'calories' => (int) $sport->calories,
            'picture' => asset($sport->picture),

        ];
    }
}