<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 6/21/16
 * Time: 18:21
 */

namespace App\Http\Transformers;


use App\Setting;
use League\Fractal\TransformerAbstract;

class SettingsTransformer extends TransformerAbstract
{
    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(Setting $setting)
    {
        return [
            'invitation_notifications'          => (bool) $setting['invitation_notifications'],
            'friend_request_notifications'     =>  (bool) $setting['friend_request_notifications'],
        ];
    }
}