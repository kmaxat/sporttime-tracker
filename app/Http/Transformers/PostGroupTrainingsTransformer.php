<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 6/14/16
 * Time: 12:47
 */

namespace App\Http\Transformers;


use App\GroupTraining;
use League\Fractal\TransformerAbstract;

class PostGroupTrainingsTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(GroupTraining $groupTraining)
    {
        return [
            'id'     => (int) $groupTraining['id'],
            'user_id' => (int)$groupTraining['user_id'],
            'sport_id' => (int)$groupTraining['sport_id'],
            'location'     => (array) $groupTraining['location'],
            'title' =>  $groupTraining['title'],
            'description' => $groupTraining['description'],
            'price' => (float) $groupTraining['price'],
            'private_state' => $groupTraining['private_state'],
            'start_date'    => $groupTraining['start_date'],
            'start_time'      => $groupTraining['start_time'],
            'end_time'      => $groupTraining['end_time'],
            'repeat_type'   => $groupTraining['repeat_type'],
            'repeat_days'   => $groupTraining['repeat_days'],
            'invites_user' => $groupTraining['invites_user'],
        ];
    }
}
{

}