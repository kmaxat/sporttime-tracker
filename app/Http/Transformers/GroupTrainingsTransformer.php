<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/20/16
 * Time: 13:23
 */

namespace App\Http\Transformers;


use App\GroupTraining;
use League\Fractal\TransformerAbstract;

class GroupTrainingsTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(GroupTraining $groupTraining)
    {
        if (!empty($groupTraining->user->picture)) {
            $picture = asset($groupTraining->user->picture);
        }
        else
            $picture = null;

        return [
            'id'     => (int) $groupTraining['id'],
            'location'     => (array) $groupTraining['location'],
            'title' =>  $groupTraining['title'],
            'description' => $groupTraining['description'],
            'price' => (float) $groupTraining['price'],
            'private_state' => $groupTraining['private_state'],
            'start_date'    => $groupTraining['start_date'],
            'start_time'      => $groupTraining['start_time'],
            'end_time'      => $groupTraining['end_time'],
            'repeat_type'   => $groupTraining['repeat_type'],
            'repeat_days'   => $groupTraining['repeat_days'],
            'count_accepted' => (int)$groupTraining['count_accepted'],
            'count_invited' => (int)$groupTraining['count_invited'],
            'count_maybe' => (int)$groupTraining['count_maybe'],
            'sport' => [
                'id' => (int) $groupTraining->sport->id,
                'title' => $groupTraining->sport->title,
                'title_eng' => $groupTraining->sport->title_eng,
                'picture' => asset($groupTraining->sport->picture),
                'calories' => (int) $groupTraining->sport->calories,
            ],
            'user' => [
                'id' => (int)$groupTraining->user->id,
                'first_name' => $groupTraining->user->first_name,
                'last_name' => $groupTraining->user->last_name,
                'picture' => $picture,
            ]
        ];
    }
}