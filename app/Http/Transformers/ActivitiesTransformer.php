<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/31/16
 * Time: 16:27
 */

namespace App\Http\Transformers;


use App\Activity;
use League\Fractal\TransformerAbstract;

class ActivitiesTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(Activity $activity)
    {
        $action = $activity->subject_action;
        if (!empty($activity->user->picture)) {
            $picture = asset($activity->user->picture);
        }
        else
            $picture = null;

        if ($action == Activity::FINISHED_TRAINING) {
            return [
                'id' => (int)$activity['id'],
                'action' => $activity['subject_action'],
                'date' => (string) $activity['created_at'],
                'user' => [
                    'id' => (int)$activity->user->id,
                    'gender' => $activity->user->gender,
                    'first_name' => $activity->user->first_name,
                    'last_name' => $activity->user->last_name,
                    'picture' => $picture,
                ],
                'training' => [
                    'id'     => (int) $activity->subject['id'],
                    'user_id'     => (int) $activity->subject['user_id'],
                    'route'     => json_decode($activity->subject['route']),
                    'calories'     => (int) $activity->subject['calories'],
                    'distance'     => (double) $activity->subject['distance'],
                    'duration'     => (int) $activity->subject['duration'],
                    'average_speed'     => (float)$activity->subject['average_speed'],
                    'average_pace'     => (float) $activity->subject['average_pace'],
                    'max_speed'     => (float) $activity->subject['max_speed'],
                    'max_pace'     => (float) $activity->subject['max_pace'],
                    'notes'     => $activity->subject['notes'],
                    'date'     => (string) $activity->subject['created_at'],
                ],
            ];
        } else if ($action == Activity::ACCEPTED_INVITATION || $action == Activity::CREATED_GROUP_TRAINING) {
            return [
                'id' => (int)$activity['id'],
                'action' => $activity['subject_action'],
                'date' => (string) $activity['created_at'],
                'user' => [
                    'id' => (int)$activity->user->id,
                    'gender' => $activity->user->gender,
                    'first_name' => $activity->user->first_name,
                    'last_name' => $activity->user->last_name,
                    'picture' => $picture,
                ],
                'group_training' => [
                    'id'     => (int) $activity->subject['id'],
                    'user_id' => (int)$activity->subject['user_id'],
                    'sport_id' => (int)$activity->subject['sport_id'],
                    'location'     => (array) $activity->subject['location'],
                    'title' =>  $activity->subject['title'],
                    'description' => $activity->subject['description'],
                    'price' => (float) $activity->subject['price'],
                    'private_state' => $activity->subject['private_state'],
                    'start_date'    => $activity->subject['start_date'],
                    'start_time'      => $activity->subject['start_time'],
                    'end_time'      => $activity->subject['end_time'],
                    'repeat_type'   => $activity->subject['repeat_type'],
                    'repeat_days'   => $activity->subject['repeat_days'],
                    'invites_user' => $activity->subject['invites_user'],
                ],
            ];
        } else if ($action == Activity::BECAME_FRIENDS) {
            if (!empty($activity->subject->picture)) {
                $subjPicture = asset($activity->subject->picture);
            }
            else
                $subjPicture = null;
            return [
                'id' => (int)$activity['id'],
                'action' => $activity['subject_action'],
                'date' => (string) $activity['created_at'],
                'user' => [
                    'id' => (int)$activity->user->id,
                    'gender' => $activity->user->gender,
                    'first_name' => $activity->user->first_name,
                    'last_name' => $activity->user->last_name,
                    'picture' => $picture,
                ],
                'friend' => [
                    'id' => (int)$activity->subject->id,
                    'first_name' => $activity->subject->first_name,
                    'last_name' => $activity->subject->last_name,
                    'picture' => $subjPicture,
                ],
            ];
        }
    }
}