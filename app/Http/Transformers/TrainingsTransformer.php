<?php
namespace App\Http\Transformers;

use App\Training;
use League\Fractal\TransformerAbstract;


/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/18/16
 * Time: 14:56
 */
class TrainingsTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(Training $training)
    {
        return [
            'id'     => (int) $training['id'],
            'user_id'     => (int) $training['user_id'],
            'route'     => json_decode($training['route']),
            'calories'     => (int) $training['calories'],
            'distance'     => (double) $training['distance'],
            'duration'     => (int) $training['duration'],
            'average_speed'     => (float)$training['average_speed'],
            'average_pace'     => (float) $training['average_pace'],
            'max_speed'     => (float) $training['max_speed'],
            'max_pace'     => (float) $training['max_pace'],
            'notes'     => $training['notes'],
            'date'     => (string) $training['created_at'],
            'sport' => [
                'id' => (int) $training->sport->id,
                'title' => $training->sport->title,
                'title_eng' => $training->sport->title_eng,
                'picture' => asset($training->sport->picture),
                'calories' => (int) $training->sport->calories,
            ]
        ];
    }
}