<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 6/13/16
 * Time: 14:50
 */

namespace App\Http\Transformers;


use App\Record;
use League\Fractal\TransformerAbstract;

class RecordTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(Record $record)
    {
        return [
            'place'     => (int) $record['place'],
        ];
    }
}