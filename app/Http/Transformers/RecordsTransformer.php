<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 6/20/16
 * Time: 12:13
 */

namespace App\Http\Transformers;


use App\Record;
use League\Fractal\TransformerAbstract;

class RecordsTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(Record $record)
    {
        if (!empty($record->user->picture)) {
            $picture = asset($record->user->picture);
        }
        else
            $picture = null;

        return [
            'id' => (int) $record->id,
            'duration_seconds' => (int) $record->duration_seconds,
            'sport' => [
                'id' => (int) $record->sport->id,
                'title' => $record->sport->title,
                'title_eng' => $record->sport->title_eng,
                'calories' => (int) $record->sport->calories,
                'picture' => asset($record->sport->picture)
            ],
            'user' => [
                'id' => (int)$record->user->id,
                'first_name' => $record->user->first_name,
                'last_name' => $record->user->last_name,
                'picture' => $picture,
            ]
        ];
    }
}