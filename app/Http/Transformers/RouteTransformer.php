<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 6/9/16
 * Time: 12:29
 */

namespace App\Http\Transformers;


use App\Route;
use League\Fractal\TransformerAbstract;

class RouteTransformer extends TransformerAbstract
{

    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param \Dingo\Api\Transformer\Binding $binding
     * @param \Dingo\Api\Http\Request $request
     *
     * @return array
     */
    public function transform(Route $route)
    {
        if (!empty($route->picture)) {
            $picture = asset($route->picture);
        }
        else
            $picture = null;

        return [
            'id'     => (int) $route['id'],
            'title'     => $route['title'],
            'distance'     => (float) $route['distance'],
            'private_state' => $route['private_state'],
            'picture'     => $picture,
            'route'     =>  json_decode($route['route']),
            'records' => $route->records,
        ];
    }
}