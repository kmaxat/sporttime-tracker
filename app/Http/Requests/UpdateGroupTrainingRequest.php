<?php

namespace App\Http\Requests;

use App\GroupTraining;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class UpdateGroupTrainingRequest extends Request
{
    protected $default  = [
        'repeat_type'  => GroupTraining::REPEAT_TYPE_NONE
    ];


    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $repeatRules    = [];
        $repeatDays     = '';
        $type           = Input::get('repeat_type', null);
        if(in_array($type, GroupTraining::$REPEAT_TYPES) && $type != GroupTraining::REPEAT_TYPE_NONE) {
            if($type == GroupTraining::REPEAT_TYPE_WEEK) {
                $range  = range(1, 7);
            }
            else {
                $range  = range(1, 31);
            }


            $repeatDays = 'array';
            $inputDays  = (array)Input::get('repeat_days');
            foreach($inputDays as $k=>$day) {
                $key    = 'repeat_days.' . $k;
                $rule   = 'in:' . implode(',', $range);

                $repeatRules[$key]  = $rule;
            }
        }

        $rules  = [
            'sport_id'          => 'exists:sports,id',
            'location.lat'      => 'numeric',
            'location.lon'      => 'numeric',
            'title'             => 'max:200',
            'description'       => 'max:1000',
            'price'             => 'numeric',
            'start_date'        => 'date_format:Y-m-d',
            'start_time'        => 'date_format:H:i',
            'end_time'          => 'date_format:H:i',
            'repeat_type'       => 'in:' . implode(',', GroupTraining::$REPEAT_TYPES),
            'repeat_days'       => $repeatDays,
            'invites_user'      => 'numeric_array'
        ];

        $rules  = array_merge($rules, $repeatRules);

        return $rules;
    }
}

Validator::extend('numeric_array', function($attribute, $values, $parameters)
{
    if(! is_array($values)) {
        return false;
    }

    foreach($values as $v) {
        if(! is_numeric($v)) {
            return false;
        }
    }

    return true;
});
