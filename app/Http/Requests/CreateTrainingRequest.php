<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateTrainingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'route' => '',
            'calories' => 'digits_between:0,10',
            'sport_id' => 'required|exists:sports,id',
            'distance' => 'digits_between:0,10',
            'duration' => 'digits_between:0,10',
            'average_speed' => 'digits_between:0,10',
            'average_pace' => 'digits_between:0,10',
            'max_speed' => 'digits_between:0,10',
            'max_pace' => 'digits_between:0,10',
            'notes' => ''
        ];
    }
}
