<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'first_name'    => 'alpha_dash|max:100',
            'last_name'     => 'alpha_dash|max:100',
            'gender'        => 'required|in:'.implode(',', User::$SEX),
            'weight'        => 'digits_between:0,6',
            'birthday'      => 'required|date',
            'height'        => 'between:0,3',
            'picture'       => 'image|max:1024',
            'unit'          => 'in:'.implode(',', User::$UNIT),
            'onesignal_id'  => 'max:255'
        ];
    }


}
