<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Route;

class PostRouteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'private_state'     => 'required|in:' . implode(',', Route::$PRIVATE_STATES),
            'distance' => 'required|max:255',
            'center.lat' => 'required',
            'center.lon' => 'required',
            'route' => 'required',
        ];
    }
}
