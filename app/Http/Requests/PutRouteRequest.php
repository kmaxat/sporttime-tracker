<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Route;

class PutRouteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'max:255',
            'private_state' => 'in:' . implode(',', Route::$PRIVATE_STATES),
        ];
    }
}