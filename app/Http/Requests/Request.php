<?php

namespace App\Http\Requests;

use Dingo\Api\Exception\ResourceException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    //
    protected $default  = [];

    protected function failedValidation(Validator $validator)
    {
        throw new ResourceException('input error', $validator->errors());
    }

    public function all($filtred = true)
    {
        $data       = parent::all();
        $data       = array_merge($this->default, $data);

        $keys       = $this->rules();
        $filtred    = [];

        foreach($keys as $key=>$rule) {
            $value  = array_get($data, $key);
            if($value !== null) {
                array_set($filtred, $key, $value);
            }
        }

        return $filtred;
    }
}
