<?php

namespace App;

use App\Facades\NotificationServiceFacade;
use Illuminate\Database\Eloquent\Model;
use NotificationService;


class Invitation extends Model
{

    protected $fillable = [
        'group_training_id',
        'sender_id',
        'receiver_id',
        'state',
        'expires_at'
    ];

    protected $dates = [
        'expires_at'
    ];

    const STATE_INVITED         = 'invited';
    const STATE_ACCEPTED    = 'accepted';
    const STATE_REJECTED    = 'rejected';
    const STATE_MAYBE       = 'maybe';

    public static $states   = [
        self::STATE_ACCEPTED,
        self::STATE_MAYBE,
        self::STATE_REJECTED,
    ];

    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id', 'id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id', 'id');
    }

    public function groupTraining()
    {
        return $this->belongsTo('App\GroupTraining');
    }

    public static function boot()
    {
        parent::boot();
        static::created(function ($invitation) {
        });
        static::updated(function ($invitation) {
            if ($invitation->state == Invitation::STATE_ACCEPTED) {
                Activity::create([
                    'subject_id' => $invitation->group_training_id,
                    'subject_type' => 'App\GroupTraining',
                    'subject_action' =>  Activity::ACCEPTED_INVITATION,
                    'user_id' => $invitation->receiver_id
                ]);

            }
        });
    }
}
