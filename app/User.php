<?php

namespace App;

use Elasticquent\ElasticquentTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SahusoftCom\EloquentImageMutator\EloquentImageMutatorTrait;

class User extends Authenticatable
{
    use ElasticquentTrait;


    protected $table = 'users';

    const ROLE_USER     = 'user';
    const ROLE_ADMIN    = 'admin';
    const MALE = 'male';
    const FEMALE = 'female';
    public static $SEX = [
        self::MALE,
        self::FEMALE
    ];
    const MI = 'mi';
    const KM = 'km';
    public static $UNIT = [
        self::MI,
        self::KM
    ];

    protected $mappingProperties = [
        'first_name' => [
            'type'      => 'string',
            'store'     => true,
            'analyzer'  => 'standard'
        ],
        'last_name'  => [
            'type'      => 'string',
            'store'     => true,
            'analyzer'  => 'standard'
        ],
        'birthday'  => [
            'enabled' => 'false',
        ]
    ];

    protected $dates = ['dob'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //TODO: Create user account with all the data.
    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'birthday',
        'picture',
        'weight',
        'height',
        'unit',
        'email',
        'role',
        'token',
        'password',
        'onesignal_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','token','role',

    ];

    public function confirmEmail()
    {
        $this->verified = true;
        $this->token = null;
        $this->save();
    }
    public function trainings()
    {
        return $this->hasMany('App\Training');
    }

    public function groupTrainings()
    {
        return $this->hasMany('App\GroupTraining');
    }

    public function routes()
    {
        return $this->hasMany('App\Route');
    }

    public function isAdmin()
    {
        return $this->role == User::ROLE_ADMIN;
    }

    public function activity()
    {
        return $this->morphMany('App\Activity', 'subject');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    public function social()
    {
        return $this->hasMany(Social::class);
    }

    public function settings()
    {
        return $this->hasOne(Setting::class);
    }
}
