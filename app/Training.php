<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $table = 'trainings';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sport_id',
        'user_id',
        'route',
        'calories',
        'distance',
        'duration',
        'average_speed',
        'average_pace',
        'max_speed',
        'max_pace',
        'date',
        'notes',
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function sport()
    {
        return $this->belongsTo('App\Sport');
    }

    public function activity()
    {
        return $this->morphMany('App\Activity', 'subject');
    }

    public static function boot()
    {
        parent::boot();
        static::created(function ($training) {
            Activity::create([
                'subject_id' => $training->id,
                'subject_action' => Activity::FINISHED_TRAINING,
                'subject_type' =>  'App\Training',
                'user_id' => $training->user_id
            ]);
        });
    }
}
