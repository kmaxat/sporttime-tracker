<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GuzzleHttp;
use Log;

class SendNotifications extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct(array $data)
    {

        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info($this->data);
        $client = new GuzzleHttp\Client();
        $res = $client->request('POST', 'https://onesignal.com/api/v1/notifications',[
            'json' => [
                'app_id' => env('ONESIGNAL_APP_ID', ''),
                'contents' => [
                    'en' => $this->data['contents']['en'],
                    'ru' => $this->data['contents']['ru']
                ],
                'include_player_ids' => $this->data['include_player_ids'],
                'data' => $this->data['data'],
            ]
        ]);
        if ($res->getStatusCode() == 200) {
            $body = json_decode($res->getBody());
            if (property_exists($body, 'errors')) {
                Log::error($body->errors);
            }
        }
        else if ($res->getStatusCode() == 400) {
            Log::error($res->getBody());
        }

    }
}
