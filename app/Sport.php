<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;
use Log;
use SahusoftCom\EloquentImageMutator\EloquentImageMutatorTrait;

class Sport extends Model
{
    protected $fillable = [
        'title',
        'title_eng',
        'picture',
        'calories'
    ];

    public static function boot()
    {
        parent::boot();
        static::created(function ($sport) {

        });
        static::updating(function ($sport) {

        });
        static::deleted(function ($sport) {
            if (File::exists(public_path($sport->picture))) {
                File::delete(public_path($sport->picture));
            }
        });
    }

}
