<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    const FINISHED_TRAINING         = 'training_finished';
    const CREATED_GROUP_TRAINING    = 'group_training_created';
    const ACCEPTED_INVITATION       = 'invitation_accepted';
    const BECAME_FRIENDS            = 'became_friends';

    protected $fillable = [
        'subject_id',
        'subject_type',
        'subject_action',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subject()
    {
        return $this->morphTo();
    }

}
