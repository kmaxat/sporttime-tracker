<?php

namespace App;

use App\Http\Transformers\GroupTrainingsTransformer;
use League\Fractal;
use Carbon\Carbon;
use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use Redis;

class GroupTraining extends Model
{
    use ElasticquentTrait;

    protected $table = 'group_trainings';

    const PRIVATE_STATE_CLOSE = 'close';
    const PRIVATE_STATE_FRIENDS = 'friends';
    public static $PRIVATE_STATES = [
        self::PRIVATE_STATE_CLOSE,
        self::PRIVATE_STATE_FRIENDS
    ];
    const REPEAT_TYPE_NONE = 'none';
    const REPEAT_TYPE_WEEK = 'week';
    const REPEAT_TYPE_MONTH = 'month';
    public static $REPEAT_TYPES = [
        self::REPEAT_TYPE_MONTH,
        self::REPEAT_TYPE_WEEK,
        self::REPEAT_TYPE_NONE
    ];

    const CHANGED_LOCATION = 'changed_location';
    const CHANGED_SPORT = 'changed_sport';
    const CHANGED_DATETIME = 'changed_datetime';
    const CHANGED_ELSE = 'changed_else';

    //Used in NotificationsService, generating payload for InboxController
    const UPDATE = 'group_training_update';
    const INVITATION = 'group_training_invitation';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $mappingProperties = [
        'user_id' => [
            'store' => true,
            'type' => 'integer'
        ],
        'sport_id' => [
            'store' => true,
            'type' => 'integer'
        ],
        'description' => [
            'type' => 'string',
            'store' => true,
            'index' => 'analyzed'
        ],

        'title' => [
            'type' => 'string',
            'store' => true,
            'index' => 'analyzed'
        ],

        'private_state' => [
            'store' => true,
            'type' => 'string',
            'index' => 'not_analyzed'
        ],

        'location' => [
            'store' => true,
            'type' => 'geo_point',
        ],

        'start_date' => [
            'store' => true,
            'type' => 'date',
            'format' => 'date'
        ],
        'start_time' => [
            'store' => true,
            'type' => 'date',
            'format' => 'hour_minute_second'
        ],
        'end_time' => [
            'store' => true,
            'type' => 'date',
            'format' => 'hour_minute_second'
        ],

        'repeat_type' => [
            'store' => true,
            'type'  => 'string',
            'index' => 'not_analyzed'
        ],
        'repeat_days' => [
            'store' => true,
            'type'  => 'integer'
        ]
    ];

    protected $fillable = [
        'sport_id',
        'location',
        'title',
        'description',
        'price',
        'private_state',

        'start_date',
        'start_time',
        'end_time',

        'repeat_type',
        'repeat_days',
        'invites_user'
    ];

    protected $casts = [
        'location' => 'array',
        'invites_user' => 'array',
        'repeat_days' => 'array'
    ];

    public $appends = [
        'countAccepted'
    ];

    public function getTypeName()
    {
        return 'group_training';
    }

    public function getCountAcceptedAttribute()
    {
        $invitations = $this->invitations()->where('state', Invitation::STATE_ACCEPTED)->get();
        return $invitations->count();
    }

    protected function castAttribute($key, $value)
    {
        if (is_null($value)) {
            return $value;
        }

        switch ($this->getCastType($key)) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'real':
            case 'float':
            case 'double':
                return (float)$value;
            case 'string':
                return (string)$value;
            case 'bool':
            case 'boolean':
                return (bool)$value;
            case 'object':
                return json_decode($value);
            case 'array':
            case 'json':
                //if data from elasticsearch - no need to decode
                return is_array($value) ? $value : json_decode($value, true);
            case 'collection':
                return new BaseCollection(json_decode($value, true));
            default:
                return $value;
        }
    }

    public function sport()
    {
        return $this->belongsTo('App\Sport');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getIndexDocumentData()
    {
        return $this->toArray();
    }

    public function invitations()
    {
        return $this->hasMany('App\Invitation');
    }

    public function activity()
    {
        return $this->morphMany('App\Activity', 'subject');
    }

    public static function boot()
    {
        parent::boot();
        static::created(function ($groupTraining) {

            if (isset($groupTraining->invites_user)) {
                $invites = $groupTraining->invites_user;
                foreach($invites as $receiverId) {
                    if($receiverId == $groupTraining->user_id){
                        $toSave = [
                            'sender_id' => $groupTraining->user_id,
                            'state' => Invitation::STATE_ACCEPTED,
                            'receiver_id' => $receiverId,
                            'group_training_id' => $groupTraining->id,
                            'expires_at' => Carbon::parse($groupTraining->end_date.' '.$groupTraining->end_time),
                        ];
                        Invitation::create($toSave);
                    }
                    else {
                        $toSave = [
                            'sender_id' => $groupTraining->user_id,
                            'state' => Invitation::STATE_INVITED,
                            'receiver_id' => $receiverId,
                            'group_training_id' => $groupTraining->id,
                            'expires_at' => Carbon::parse($groupTraining->end_date.' '.$groupTraining->end_time),
                        ];
                        Invitation::create($toSave);

                        $fractal = new Manager();
                        $fractal->setSerializer(new ArraySerializer());

                        $groupTraining = GroupTraining::findOrFail($groupTraining->id);
                        $groupTrainingResource = new Item($groupTraining, new GroupTrainingsTransformer());
                        $groupTrainingJson = $fractal->createData($groupTrainingResource)->toJson();

                        $date = Carbon::now();
                        $payload = json_encode(array(
                                GroupTraining::INVITATION => ['group_training' => $groupTrainingJson],
                                'date'  => $date->toDateTimeString()
                        ));
                        Notification::create([
                            'user_id' => $receiverId,
                            'payload' => $payload
                        ]);
                    }
                }
            }

            Activity::create([
                'subject_id' => $groupTraining->id,
                'subject_action' => Activity::CREATED_GROUP_TRAINING,
                'subject_type' =>  'App\GroupTraining',
                'user_id' => $groupTraining->user_id
            ]);
        });

        static::updated(function ($groupTraining) {
            if (isset($groupTraining->invites_user)) {
                if ($groupTraining->isDirty('location')) {
                    $field = GroupTraining::CHANGED_LOCATION;
                } else if ($groupTraining->isDirty('sport_id')) {
                    $field = GroupTraining::CHANGED_SPORT;
                } else if ($groupTraining->isDirty('start_date') ||
                    $groupTraining->isDirty('start_time') ||
                    $groupTraining->isDirty('end_time')) {
                    $field = GroupTraining::CHANGED_DATETIME;
                } else
                    $field = GroupTraining::CHANGED_ELSE;

                $fractal = new Manager();
                $fractal->setSerializer(new ArraySerializer());

                $groupTraining = GroupTraining::findOrFail($groupTraining->id);
                $groupTrainingResource = new Item($groupTraining, new GroupTrainingsTransformer());
                $groupTrainingJson = $fractal->createData($groupTrainingResource)->toJson();

                $date = Carbon::now();
                $payload = json_encode(array(
                    GroupTraining::UPDATE => [
                        'group_training' => $groupTrainingJson,
                        'updated_field' => $field
                    ],
                    'date'  => $date->toDateTimeString()
                ));
                foreach ($groupTraining->invites_user as $invitedUser) {
                    Notification::create([
                        'user_id' => $invitedUser,
                        'payload' => $payload
                    ]);
                }
            }

        });
        static::deleting(function($groupTraining) {
            Activity::where('subject_id', $groupTraining->id)->where('subject_action', Activity::CREATED_GROUP_TRAINING)
                ->delete();
            Invitation::where('group_training_id', $groupTraining->id)->delete();
        });

    }

}
