<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/20/16
 * Time: 14:51
 */

namespace App\Services;


use App\Setting;
use App\GroupTraining;
use App\Invitation;
use App\User;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class GroupTrainingService
{
    use Helpers;
    public function getTrainings($data,$limit)
    {
        $lat    = $data['lat'];
        $lon    = $data['lon'];

        $now    = isset($data['date']) ? Carbon::createFromFormat('Y-m-d', $data['date']) : Carbon::now();
        $dayOfWeek  = (int)$now->format('N');
        $dayOfMonth = (int)$now->format('d');

        $query  = [
            'filtered'   => [
                'filter'    => [
                    'bool'  => [
                        'must'  => [
                            [
                                'term'  => [
                                    'private_state' => GroupTraining::PRIVATE_STATE_FRIENDS
                                ],
                            ],
							[
								'range' => [
									'start_date'    => [
                                        'lte' => $now->toDateString(),
									]
								]
							],

                        ],
                        'should'    => [
                            [
                                'bool'  => [
                                    'must'  => [
                                        [
                                            'term'  => [
                                                'repeat_type'   => GroupTraining::REPEAT_TYPE_NONE
                                            ]
                                        ],
                                        [
                                            'term'  => [
                                                'start_date'    => $now->format('Y-m-d')
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'bool'  => [
                                    'must'  => [
                                        [
                                            'term'  => [
                                                'repeat_type'   => GroupTraining::REPEAT_TYPE_WEEK
                                            ]
                                        ],
                                        [
                                            'term'  => [
                                                'repeat_days'   => $dayOfWeek
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'bool'  => [
                                    'must'  => [
                                        [
                                            'term'  => [
                                                'repeat_type'   => GroupTraining::REPEAT_TYPE_MONTH
                                            ]
                                        ],
                                        [
                                            'term'  => [
                                                'repeat_days'   => $dayOfMonth
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $sort = [
            'start_time' => 'asc',
            '_geo_distance'  => [
                'location'      => [
                    'lat' => $lat,
                    'lon' => $lon,
                    ],
                'order' => 'asc',
                'distance_type' => 'plane'
            ],
        ];
        $trainings  = GroupTraining::searchByQuery($query, null, null, null, 0, $sort);

        $ids        = [];
        foreach(collect($trainings) as $t) {
            $ids[]  = $t->id;
        }
        $groupTrainings = GroupTraining::whereIn('id', $ids)->paginate($limit);
        $groupTrainings->load(['sport']);
        return $groupTrainings;
    }

    public function create(User $user, $data)
    {
        $groupTraining   = $user->groupTrainings()->create($data);
        $userIds = Setting::whereIn('user_id', $groupTraining->invites_user)
            ->where('invitation_notifications', true)
            ->select('user_id')
            ->get();
        if (count($userIds)) {

            $usersOneSignalId = User::whereIn('id', $userIds)
                ->select('onesignal_id')
                ->get();
            
            $notificationData = [
                'sender' => $user->first_name.' '.$user->last_name,
                'onesignal_ids' => collect($usersOneSignalId)->pluck('onesignal_id')->toArray(),
                'title' => $groupTraining->title,
                'id' => $groupTraining->id,
            ];
            \NotificationService::queue(GroupTraining::INVITATION, $notificationData);
        }

        GroupTraining::putMapping($ignoreConflicts = true);
        $groupTraining = GroupTraining::find($groupTraining->id);
        $groupTraining->addToIndex();
        return $groupTraining;
    }

    public function getUserTrainings($userId, $limit)
    {
        $groupTrainings = $this->user->groupTrainings()
            ->orWhereIn('id', DB::table('invitations')
                ->select('invitations.group_training_id')
                ->where('state', Invitation::STATE_ACCEPTED)
                ->where('invitations.receiver_id',$userId)
            )
            ->orderBy('start_date', 'desc')
            ->orderBy('start_time', 'desc')
            ->orderBy('id', 'desc')
            ->paginate($limit);
        return $groupTrainings;
    }



    public function update($trainingId, $data)
    {
        try {
            $groupTraining = $this->user()->groupTrainings()->findOrFail($trainingId);
        } catch (ModelNotFoundException $e) {
            abort(400, 'No group training found with specified id');
        }
        $groupTraining->fill($data);
        $groupTraining->save();
        GroupTraining::putMapping($ignoreConflicts = true);
        $groupTraining = GroupTraining::find($groupTraining->id);
        $groupTraining->addToIndex();
        return $groupTraining;
    }

    public function delete($trainingId)
    {
        try {
            $groupTraining = $this->user()->groupTrainings()->findOrFail($trainingId);
        } catch (ModelNotFoundException $e) {
            abort(400, 'No group training found with specified id');
        }
        try {
            $groupTraining->removeFromIndex();
        } catch (\Exception $e) {
        }
        $groupTraining->delete();
    }

    public function setCountsAttribute(GroupTraining $groupTraining, $state, $groupCounts)
    {
        if (empty($groupCounts)) {
            $groupTraining->setAttribute($state, 0);
        }
        else
            $groupTraining->setAttribute($state, $groupCounts[0]->total);
    }

}