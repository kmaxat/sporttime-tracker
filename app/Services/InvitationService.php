<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/24/16
 * Time: 22:48
 */

namespace App\Services;


use App\Invitation;
use App\Notification;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InvitationService
{

    public function getTrainingsMeAndParticipateMeCount($type)
    {
        $groupTrainingsCount = DB::table('invitations')
            ->select('group_training_id', DB::raw('count(state) as total'))
            ->where('state', $type)
            ->orderBy('id', 'asc')
            ->groupBy('group_training_id');
            return $groupTrainingsCount->get();
    }

    public function getInvitationCountsByTrainingId($type,$id)
    {
        $groupTrainingsCount = DB::table('invitations')
            ->select('group_training_id',DB::raw('count(state) as total'))
            ->where('state', $type)
            ->where('group_training_id',$id)
            ->orderBy('id', 'asc')
            ->groupBy('group_training_id');
        return $groupTrainingsCount->get();
    }

    public function decision($userId, $id, $type)
    {
        $invitation =  Invitation::where('group_training_id', $id)
            ->where('receiver_id', $userId)
            ->firstOrFail();
        $invitation->state = $type;

        //Delete that activity from index (Not good)
        $notifications = Notification::where('user_id', $userId)->get();
        foreach ($notifications as $notification) {
            $payload = json_decode($notification->payload);
            if (isset($payload->group_training_invitation)) {
                $groupTraining = json_decode(
                    $payload->group_training_invitation->group_training
                );
                if (intval($groupTraining->id) === intval($id)) {
                    $notification->delete();
                }
            }

        }
        return $invitation->save();
    }

}