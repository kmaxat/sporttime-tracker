<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/31/16
 * Time: 16:44
 */

namespace App\Services;


use App\Friend;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FriendService
{
    public function getFriends($userId)
    {
        $friends = Friend::where(function ($query) use ($userId) {
            $query->where('sender_id', $userId)
                ->where('status', Friend::ACCEPTED);
        })
            ->orWhere(function ($query) use ($userId) {
                $query->where('receiver_id', $userId)
                    ->where('status', Friend::ACCEPTED);
        })->get();

        return $friends;
    }

    public function getFriendsIds($friends, $userId)
    {
        $friendsIDs = array();
        foreach ($friends as $friend) {
            if ($friend->receiver_id == $userId)
                $friendsIDs[] = $friend->sender_id;
            else
                $friendsIDs[] = $friend->receiver_id;
        }
        return $friendsIDs;
    }

    public function findExistingRequests($userId, $id)
    {
        return Friend::where(function ($query) use ($userId,$id) {
            $query->where('sender_id',$userId)
                ->where('receiver_id',$id);
        })
            ->orWhere(function ($query) use ($userId,$id) {
                $query->where('sender_id', $id)
                    ->where('receiver_id', $userId);
            })
            ->first();
    }

    public function checkIfFriends($currUser, $friendsID)
    {
        try {
            Friend::where(function ($query) use ($currUser,$friendsID) {
                $query->where('sender_id', $currUser)
                    ->where('receiver_id',$friendsID)
                    ->where('status', Friend::ACCEPTED);
            })
                ->orWhere(function ($query) use ($currUser,$friendsID) {
                    $query->where('sender_id', $friendsID)
                        ->where('receiver_id',$currUser)
                        ->where('status', Friend::ACCEPTED);
                })->firstOrFail();
        } catch (ModelNotFoundException $e) {
            abort(401, 'Not authorized to view activity feed');
        }
    }
}