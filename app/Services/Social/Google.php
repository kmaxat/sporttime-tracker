<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/13/16
 * Time: 15:11
 */

namespace App\Services\Social;
use GuzzleHttp;
use GuzzleHttp\Exception\RequestException;

class Google
{
    public function getGoogleUserData($token)
    {
        try {
            $client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer '.$token]]);
            $url = 'https://www.googleapis.com/plus/v1/people/me';
            $response   = $client->get($url);
            $data       = json_decode($response->getBody()->getContents());
            if (isset($data->emails)){
                foreach ($data->emails as $email) {
                    if($email->type == 'account'){
                        $userEmail = $email->value;
                    }
                }
            }
            $userData     = [
                'social_id'  => isset($data->id) ? $data->id : '',
                'first_name'  => isset($data->name->givenName) ? $data->name->givenName : '',
                'last_name'  => isset($data->name->familyName) ? $data->name->familyName : '',
                'picture'  => isset($data->image->url) ? $data->image->url : '',
                'gender'  => isset($data->gender) ? $data->gender : '',
                'email' => isset($userEmail) ? $userEmail : md5(time()),
            ];
            return $userData;
        }
        catch(RequestException $e) {
            abort(400, 'Social token is invalid');
        }
    }
}