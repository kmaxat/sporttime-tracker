<?php
namespace App\Services\Social;
use App\Services\UserServices;
use Carbon\Carbon;
use GuzzleHttp;
use GuzzleHttp\Exception\RequestException;


/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/11/16
 * Time: 12:10
 */
class Facebook
{
    public function getFacebookUserData($token)
    {
        try {
            $client = new GuzzleHttp\Client();
            $url        = 'https://graph.facebook.com/v2.6/me?fields=email,first_name,last_name,'.
                'picture,gender,birthday&access_token=' . $token;
            $response   = $client->get($url);
            $data       = json_decode($response->getBody()->getContents());
            $userData     = [
                'social_id'  => isset($data->id) ? $data->id : '',
                'first_name'  => isset($data->first_name) ? $data->first_name : '',
                'last_name'  => isset($data->last_name) ? $data->last_name : '',
                'picture'  => isset($data->picture->data->url) ? $data->picture->data->url : '',
                'gender'  => isset($data->gender) ? $data->gender : '',
                'birthday'  => isset($data->birthday) ? Carbon::parse($data->birthday) : '',
                'email' => isset($data->email) ? $data->email : md5(time()),
            ];
            return $userData;
        }
        catch(RequestException $e) {
            abort(400, 'Social token is invalid');
        }
    }
}