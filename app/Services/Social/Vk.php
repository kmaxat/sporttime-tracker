<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/12/16
 * Time: 16:24
 */

namespace App\Services\Social;
use Carbon\Carbon;
use GuzzleHttp;
use GuzzleHttp\Exception\RequestException;
use InvalidArgumentException;


class Vk
{
    public function getVKUserData($user_id)
    {
        try {
            $client = new GuzzleHttp\Client();
            $url = 'https://api.vk.com/method/users.get?fields=first_name, email, last_name,'.
                'sex,photo_big,bdate&user_ids='.$user_id;
            $response   = $client->get($url);
            $responseData       = json_decode($response->getBody()->getContents());
            //Decoding VK JSON Structure
            $data = $responseData->response[0];
            //Sex integer to  enum
            if(isset($data->sex)) {
                if ($data->sex == '1')
                    $gender = 'female';
                else if ($data->sex == '2')
                    $gender = 'male';
            }
            //Catching errors if not full birthday is included.
            if(isset($data->bdate)){
                try {
                    $bdate = Carbon::createFromFormat('d.m.Y', $data->bdate);
                }
                catch(InvalidArgumentException $e) {
                    $bdate = '';
                }
            }

            $userData     = [
                'social_id'  => isset($data->uid) ? $data->uid : '',
                'first_name'  => isset($data->first_name) ? $data->first_name : '',
                'last_name'  => isset($data->last_name) ? $data->last_name : '',
                'picture'  => isset($data->photo_big) ? $data->photo_big : '',
                'gender'  => isset($gender) ? $gender : '',
                'birthday'  => isset($bdate) ? $bdate : '',
                'email' => isset($data->email) ? $data->email : md5(time()),
            ];
            return $userData;
        }
        catch(RequestException $e) {
            abort(400, 'Social token is invalid');
        }
    }
}

