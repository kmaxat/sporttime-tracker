<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/11/16
 * Time: 16:04
 */

namespace App\Services;


use App\Friend;
use App\Http\Requests\UpdateRequest;
use App\Setting;
use App\Social;
use App\User;
use Carbon\Carbon;
use DB;
use Dingo\Api\Contract\Http\Request;
use Dingo\Api\Routing\Helpers;
use File;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;

class UserService
{
    use  Helpers;

    public function createUser(array $data){

        if (array_get($data,'picture')) {
            $picture = array_get($data,'picture');
            $now = Carbon::now();
            $path ='images/users/'.$now->year.'/'.$now->month.'/'.$now->day.'/';

            if (!File::exists(public_path($path))) {
                File::makeDirectory(public_path($path), 493, true, false);
            }
            if (is_string($picture)) {
                $img = Image::make($picture);
                $picture = preg_replace('/\?.*/', '', $picture);
                $filename = uniqid('img_').'.'.pathinfo($picture, PATHINFO_EXTENSION);
                $img->save($path.$filename);

            } else {
                $filename = uniqid('img_').'.'.$picture->getClientOriginalExtension();
                $picture->move(public_path($path), $filename);
            }

        } else {
            $path = '';
            $filename = '';
        }

        $user   = User::create([
            'role'      => array_get($data, 'role', ''),
            'email'     => array_get($data, 'email', ''),
            'first_name'     => array_get($data, 'first_name', ''),
            'last_name'     => array_get($data, 'last_name', ''),
            'gender'     => array_get($data, 'gender', ''),
            'birthday'     => array_get($data, 'birthday', ''),
            'picture'     => $path.$filename,
            'unit' => array_get($data,'unit','km'),
            'password'  => isset($data['password']) ? bcrypt($data['password']) : '',
            'token' => str_random(30),
        ]);
        User::putMapping($ignoreConflicts = true);
        $userES = User::findOrFail($user->id);
        $userES->addToIndex();

        Setting::create(['user_id' => $user->id]);

        return $user;
    }

    public function createUserBySocial($socialName, array $data = [])
    {
        $social = Social::where('social_name', $socialName)
            ->where('social_id', array_get($data,'social_id'))
            ->first();
        if ($social) {
            $user = User::findOrFail($social->user_id);
            return $user;
        } else {
            $data['role'] = User::ROLE_USER;
            $user = User::where('email', array_get($data,'email'))->first();
            if (!$user) {
                $user = $this->createUser($data);
            }
            //TODO: Add update data by social login
            Social::create([
                'user_id' => $user->id,
                'social_name' => $socialName,
                'social_id' => array_get($data,'social_id')
            ]);

            User::putMapping($ignoreConflicts = true);
            $userES = User::findOrFail($user->id);
            $userES->addToIndex();

            return $user;
        }
    }

    public function sendConfirmationEmail($user)
    {
        Mail::queue('emails.confirm', compact('user'), function ($message) use ($user) {
            $message->to($user->email);
            $message->subject('Подтверждение емайла в SportTime Club');
            $message->from('temirzhan@sporttimeapp.com', 'Temirzhan from SportTimeApp');
        });

    }

    public function update($id,UpdateRequest $request)
    {
        $user   = User::find($id);
        $data = $request->all();
        if ($request->hasFile('picture')) {
            if (File::exists(public_path($user->picture))) {
                File::delete(public_path($user->picture));
            }
            $picture = $request->file('picture');
            $now = Carbon::now();
            $path = 'images/users/' . $now->year . '/' . $now->month . '/' . $now->day . '/';
            if (!File::exists(public_path($path))) {
                File::makeDirectory(public_path($path), 493, true, false);
            }
            $filename = uniqid('img_') . '.' . $picture->getClientOriginalExtension();
            $picture->move(public_path($path), $filename);
            $data['picture'] = $path . $filename;
        }
        $user->fill($data);
        $user->save();

        User::putMapping($ignoreConflicts = true);
        $userES = User::findOrFail($user->id);
        $userES->addToIndex();
        return $userES;
    }

    public function changePassword($user, $old, $new)
    {
        if ($user->social()->first()) {
            if (isset($user->password) && strlen($user->password) < 6) {
                $user->password = Hash::make($new);
                $user->save();
                return;
            }
        }
        if(!Hash::check($old, $user->password)) {
            abort(401,'Old password is wrong');
        }
        $user->password = Hash::make($new);
        $user->save();
    }

    public function search($q,$limit)
    {
        $query  = [
            'query_string' => [
                'query' => "*".$q."*",
                'fields' => [ "first_name","last_name"]
            ]
        ];
        $users = User::searchByQuery($query);
        $ids        = [];
        foreach(collect($users) as $u) {
            $ids[]  = $u->id;
        }
        $userId = $this->user()->id;
        $searchedUsers = User::whereIn('id', $ids)
            ->whereNotIn('id', [$userId])->paginate($limit);

        foreach ($searchedUsers as $user) {
               $friends = Friend::where(function ($query) use ($userId, $user) {
                $query->where('sender_id',$userId)
                    ->where('receiver_id',$user->id);
            })
                ->orWhere(function ($query) use ($userId, $user) {
                    $query->where('sender_id', $user->id)
                        ->where('receiver_id', $userId);
                })
                ->first();
            if (isset($friends)) {
                if($friends->status == Friend::ACCEPTED)
                    $user->setAttribute('friends', 'accepted');
                else
                    $user->setAttribute('friends', 'requested');
            }
        }
        return $searchedUsers;
    }

    public function findUserbyId($id)
    {
        try {
           $user =  User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(400, 'No user found with specified id');
        }
        return $user;
    }


}