<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/17/16
 * Time: 17:43
 */

namespace App\Services;


use App\Training;

class TrainingService
{
    public function find($id)
    {
        $trainings  = Training::find($id);

        return $trainings;
    }
}