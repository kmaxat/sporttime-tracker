<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/24/16
 * Time: 18:39
 */

namespace App\Services;

//use
use App\Friend;
use App\GroupTraining;
use App\Jobs\SendNotifications;
use App\Setting;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Log;

class NotificationService
{
    use DispatchesJobs;


    public function queue($type, array $data)
    {
        if ($type == Friend::FRIEND_REQUEST) {
            $userID = Setting::where('user_id', $data['receiver_id'])
                ->where('friend_request_notifications', true)
                ->select('user_id')
                ->get();
            if (count($userID)) {
                $usersOneSignalId = User::whereIn('id', $userID)->firstOrFail();
                $data = [
                    'contents' => [
                        'en' => $data['sender'].' sent you a friend request',
                        'ru' => $data['sender'].' отправил вам запрос на дружбу'
                    ],
                    'include_player_ids' => [$usersOneSignalId->getAttribute('onesignal_id')],
                    'data' => [],
                ];
                if (strlen($usersOneSignalId->onesignal_id) > 2) {
                    $this->dispatch(new SendNotifications($data));
                }
            }

        } else if ($type == GroupTraining::INVITATION) {

            $usersOneSignalId = collect($data['onesignal_ids'])->filter(function ($value){
                return strlen($value) > 2;
            });

            $notificationsData = [
                'contents' => [
                    'en' => $data['sender'].' invited you to training "'.$data['title'].'"',
                    'ru' => $data['sender'].' пригласил вас на тренировку "'.$data['title'].'"'
                ],
                'include_player_ids' => $usersOneSignalId->toArray(),
                'data' => [
                    'group_training_id' => $data['id'],
                ]

            ];
            if($usersOneSignalId->count()>0)
                $this->dispatch(new SendNotifications($notificationsData));


        }
    }

}