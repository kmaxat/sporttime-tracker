<?php
namespace App\Facades;
use Illuminate\Support\Facades\Facade;

/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/11/16
 * Time: 17:09
 */
class UserServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Services\UserService';
    }
}