<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/26/16
 * Time: 01:49
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class InvitationServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
{
    return 'App\Services\InvitationService';
}
}