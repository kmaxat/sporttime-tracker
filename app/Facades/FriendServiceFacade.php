<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/31/16
 * Time: 16:47
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class FriendServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Services\FriendService';
    }
}