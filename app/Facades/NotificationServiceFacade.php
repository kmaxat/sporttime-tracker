<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/24/16
 * Time: 18:41
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class NotificationServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Services\NotificationService';
    }
}