<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/20/16
 * Time: 15:23
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class GroupTrainingServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Services\GroupTrainingService';
    }
}