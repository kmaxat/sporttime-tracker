<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    //
    protected $fillable = [
        'title',
        'distance',
        'route',
        'private_state',
        'picture',
        'user_id'
    ];
    const PRIVATE_STATE_CLOSE = 'close';
    const PRIVATE_STATE_FRIENDS = 'friends';
    public static $PRIVATE_STATES = [
        self::PRIVATE_STATE_CLOSE,
        self::PRIVATE_STATE_FRIENDS
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function boot()
    {
        parent::boot();
        static::created(function ($route) {

        });
        static::updating(function ($route) {

        });
        static::deleted(function ($route) {
            if (File::exists(public_path($route->picture))) {
                File::delete(public_path($route->picture));
            }
        });
    }
}
