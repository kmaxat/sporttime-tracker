<!DOCTYPE html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <title>О нас | SportTime Club</title>
    <meta property="og:title" content="Sport Time App"/>
    <meta property="og:site_name" content="Sport time"/>
    <meta property="og:description" content="SportTime Club — приложение для организации и поиска cовместных спортивных тренировок"/>
    <meta property="og:image" content=" "/>

    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="css/style.css">

    <style>
        body {
            background: #e9e9e9;
        }
    </style>
</head>
<body>
<a href="index.html" class="logo logo-dark"></a>
<div class="content">
    <div class="col-12">
        <h1>О приложении</h1>
        <div class="photo">
            <img src="img/photo.jpg">
        </div>
        <div class="desc">
            <h2>Temirzhan Abdrakhmanov</h2>
            <p>
                App owner (Создатель приложения)
                entrepreneur, triathlete, cyclist, co-founder of AlmatyBroTeam, co-founder of Kazakhstan Amateur Triathlon Federation
            </p>

            <p>
                <a href="mailto:temtrade@gmail.com">temtrade@gmail.com</a>
            </p>
        </div>
    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
<script src="js/jquery.fullpage.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.contenttoggle.js"></script>
    <script>
        $('.js-contentToggle').contentToggle({
            independent: true,
            toggleOptions : {
                duration: 400
            }
        });
    </script>
</body>
</html>
