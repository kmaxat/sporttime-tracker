<!DOCTYPE html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <title>FAQ | SportTime Club</title>
    <meta property="og:title" content="Sport Time App"/>
    <meta property="og:site_name" content="Sport time"/>
    <meta property="og:description" content="SportTime Club — приложение для организации и поиска cовместных спортивных тренировок"/>
    <meta property="og:image" content=" "/>

    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="css/style.css">

    <style>
        body {
            background: #e9e9e9;
        }
    </style>
</head>
<body>

<a href="index.html" class="logo logo-dark"></a>

<div class="content">
    <div class="col-12">
        <h1>Часто задаваемые вопросы</h1>

        <ul class="accordion">
            <li class="accordion__item js-contentToggle">
                <h2 class="accordion__trigger js-contentToggle__trigger" type="button">Как скачать приложение?</h2>
                <div class="accordion__content is-hidden js-contentToggle__content">
                    <p>Ссылки на скачивание приложения находится в футере сайта</p>
                </div>
            </li>
        </ul>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
<script src="js/jquery.fullpage.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.contenttoggle.js"></script>
<script>
    $('.js-contentToggle').contentToggle({
        independent: true,
        toggleOptions : {
            duration: 400
        }
    });
</script>






</body>
</html>
