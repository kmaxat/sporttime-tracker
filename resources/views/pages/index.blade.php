<!DOCTYPE html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <title>SportTime Club — приложение для организации и поиска cовместных спортивных тренировок</title>
    <meta property="og:title" content="Sport Time App"/>
    <meta property="og:site_name" content="Sport time"/>
    <meta property="og:description" content="SportTime Club — приложение для организации и поиска cовместных спортивных тренировок"/>
    <meta property="og:image" content=" "/>

    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>

<a href="{{ route('home.index') }}" class="logo"></a>



<div id="fullpage">
    <div class="section slide1">

        <div class="content">
            <div class="col-5" id="port">
                {{--<img class="parallax-layer" src="img/1.png" alt="" style="width:600px; height:400px;"/>--}}
                {{--<img class="parallax-layer" src="img/2.png" alt="" style="width:570px; height:380px;"/>--}}
                {{--<img class="parallax-layer" src="img/3.png" alt="" style="width:540px; height:360px;"/>--}}
            </div>

            <div class="col-5 first-text">
                <h1>SportTime Club — приложение для организации и поиска cовместных спортивных тренировок</h1>
                <p>
                    Вы увлекаетесь спортом и вы всегда думали, что организацию спортивных тренировок можно сделать проще? SportTime Club - приложение для организации и поиска cовместных спортивных тренировок. SportTime Club связывает вас с местными спортсменами для удобного процесса создания совместных тренировок.
                </p>

                <a class="btn" href="#" target="_blank">
                    <div class="btn-icon"><span class="icon-playmarket"></span></div>
                    <div class="btn-text">
                        Скачать приложение <br>для Android
                    </div>
                </a>

                <a class="btn" href="#" target="_blank">
                    <div class="btn-icon"><span class="icon-appstore"></span></div>
                    <div class="btn-text">
                        Скачать приложение <br>для Iphone
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="section slide2">

        <div class="side-menu">
            <a href="#slide1" class="active"><span></span></a>
            <a href="#slide2"><span></span></a>
            <a href="#slide3"><span></span></a>
            <a href="#slide4"><span></span></a>
            <a href="#slide5"><span></span></a>
        </div>

        <div class="content">
            <div class="col-5 left-img">
                <img src="{{ secure_asset('img/slide1.png')}}" alt="">
            </div>
            <div class="col-5">
                <h1>SportTime Club позволяет:</h1>

                <p>
                    Организовывать и приглашать игроков на спортивные мероприятия
                </p>
            </div>
        </div>
    </div>
    <div class="section slide3">

        <div class="side-menu">
            <a href="#slide1"><span></span></a>
            <a href="#slide2" class="active"><span></span></a>
            <a href="#slide3"><span></span></a>
            <a href="#slide4"><span></span></a>
            <a href="#slide5"><span></span></a>
        </div>

        <div class="content">
            <div class="col-5 left-img">
                <img src="{{secure_asset('img/slide2.png')}}" alt="">
            </div>
            <div class="col-5">
                <h1>SportTime Club позволяет:</h1>

                <p>
                    Найти и участововать в тренировке поблизости более чем по 60 видам спорта
                </p>

                <img src="{{ secure_asset('img/sports.png')}}">
            </div>
        </div>
    </div>
    <div class="section slide4">

        <div class="side-menu">
            <a href="#slide1"><span></span></a>
            <a href="#slide2"><span></span></a>
            <a href="#slide3" class="active"><span></span></a>
            <a href="#slide4"><span></span></a>
            <a href="#slide5"><span></span></a>
        </div>

        <div class="content">
            <div class="col-5 left-img">
                <img src="{{ secure_asset('img/slide3.png') }} " alt="">
            </div>
            <div class="col-5">
                <h1>SportTime Club позволяет:</h1>

                <p>
                    Подписаться на спортивную активность друзей и знакомых
                </p>
            </div>
        </div>
    </div>
    <div class="section slide5">

        <div class="side-menu">
            <a href="#slide1"><span></span></a>
            <a href="#slide2"><span></span></a>
            <a href="#slide3"><span></span></a>
            <a href="#slide4" class="active"><span></span></a>
            <a href="#slide5"><span></span></a>
        </div>

        <div class="content">
            <div class="col-5 left-img">
                <img src="{{ secure_asset('img/slide4.png') }} " alt="">
            </div>
            <div class="col-5">
                <h1>SportTime Club позволяет:</h1>

                <p>
                    Получать уведомления о созданных тренировках с вашим любимым спортом.
                </p>

                <p>
                    Исследовать новые места тренировок и узнавать, что происходит вокруг вас
                </p>
            </div>
        </div>
    </div>
    <div class="section slide6">

        <div class="side-menu">
            <a href="#slide1"><span></span></a>
            <a href="#slide2"><span></span></a>
            <a href="#slide3"><span></span></a>
            <a href="#slide4"><span></span></a>
            <a href="#slide5" class="active"><span></span></a>
        </div>

        <div class="content">
            <div class="col-5 left-img">
                <img src="{{ secure_asset('img/slide5.png') }} " alt="">
            </div>
            <div class="col-5">
                <h1>SportTime Club позволяет:</h1>

                <p>
                    Рассказать друзьям в социальных сетях о вашей спортиной деятельности
                </p>
            </div>
        </div>
    </div>
    <div class="section slide7">
        <div class="content">

            <div class="download">
                <h1>Загрузить приложение</h1>

                <a class="btn" href="#" target="_blank">
                    <div class="btn-icon"><span class="icon-playmarket"></span></div>
                    <div class="btn-text">
                        Скачать приложение <br>для Android
                    </div>
                </a>

                <a class="btn" href="#" target="_blank">
                    <div class="btn-icon"><span class="icon-appstore"></span></div>
                    <div class="btn-text">
                        Скачать приложение <br>для Iphone
                    </div>
                </a>
            </div>

            <div class="donate">
                <a href="#" class="donate-btn"></a>
            </div>

            <div class="menu">
                <p><a href="{{ route('home.faq') }}">Часто задаваемые вопросы</a></p>
                <p><a href="{{ route('home.about') }}">О нас</a></p>
            </div>

            <a class="iphone"></a>
            <a class="nexus"></a>

            <div class="copyright">
                © Sport Time, 2015
            </div>

        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
<script src="{{ asset('js/jquery.fullpage.min.js')}}"></script>
{{--<script src="{{ asset('js/jquery.parallax.min.js') }}"></script>--}}



<script>
    $(document).ready(function() {
        $('#fullpage').fullpage({
            anchors:['index', 'slide1', 'slide2', 'slide3', 'slide4', 'slide5', 'download'],
            verticalCentered: false,
            responsiveWidth: 940
        });

        jQuery('.parallax-layer').parallax({
            mouseport: jQuery(".slide1")
        });

    });
</script>





</body>
</html>