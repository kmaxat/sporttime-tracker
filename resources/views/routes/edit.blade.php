@extends('layouts.app')
@include('layouts.navbar')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="form-group col-md-6">
                        {!! Form::label('email', 'Название маршрута', array('class' => 'control-label')) !!}
                        {!! Form::text('title',$route->title,array('id'=>'title', 'class' => 'form-control')) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="form-group col-md-10">
                            {!! Form::label('email', 'Маршрут', array('class' => 'control-label')) !!}
                        </div>
                        <div class="form-group col-md-10">
                            <div class="btn-group" role="group" aria-label="...">
                                <button type="button" onclick="deleteMarkers();" class="btn btn-default">Начать заново</button>
                                <button type="button" onclick="popMarkers();" class="btn btn-danger">Удалить последную точку</button>
                            </div>

                            <h4 class="text-center">Дистанция <span id="distance">{{$route->distance}}</span></h4>
                        </div>
                        <div class="form-group col-md-10">
                            <div style="width:100%; height:500px;" id="map"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <button id="submit-route" type="submit" class="btn btn-success">
                            <i class="fa fa-plus"></i> Сохранить
                        </button>
                    </div>
                </div>


                <script>
                    var map;
                    var markers = [];
                    var poly;
                    var path = [];
                    var route = {!! $route->route !!};
                    var picture;
                    var distance;

                    function initMap() {
                        var location = {lat: 43.397, lng: 76.644};

                        map = new google.maps.Map(document.getElementById('map'), {
                            center: location,
                            zoom: 14
                        });

                        poly = new google.maps.Polyline({
                            strokeColor: '#1E90FF',
                            strokeOpacity: 1.0,
                            strokeWeight: 3
                        });
                        poly.setMap(map);

                        // Try HTML5 geolocation.
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                var pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude
                                };

                                map.setCenter(pos);
                            }, function() {
                                handleLocationError(true, infoWindow, map.getCenter());
                            });
                        } else {
                            // Browser doesn't support Geolocation
                            handleLocationError(false, infoWindow, map.getCenter());
                        }

                        path = poly.getPath();
                        for (var i = 0; i < route.length; i++) {
                            myLatLng = new google.maps.LatLng(route[i]);
                            var marker = new google.maps.Marker({
                                position: myLatLng,
                                map: map,
                            });
                            markers.push(marker);
                            path.push(myLatLng);

                            location = {
                                lat: marker.getPosition().lat(),
                                lng: marker.getPosition().lng()
                            };

                        }

                    }

                    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                        infoWindow.setPosition(pos);
                        infoWindow.setContent(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                                'Error: Your browser doesn\'t support geolocation.');
                    }

                    $(document).ready(function(){
                        $('#submit-route').click(function(){
                            console.log(picture);
                            console.log(distance);
                            console.log(route);
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN' : "{{ json_encode(csrf_token()) }}",
                                },
                                url: '/routes/{{$route->id}}',
                                method: "PUT",
                                data: {
                                    'title': $('#title').val(),
                                    '_token':"{{ csrf_token() }}"
                                },
                                success: function(result){
                                    window.location.replace("/routes");
                                },
                                fail: function(result){
                                    alert(result);
                                },

                            });
                        });
                    });
                </script>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3PUzj-kqVfJAZim2l7pCfHovOsREa-EA&libraries=geometry&callback=initMap" async defer></script>
            </div>
        </div>
    </div>
@endsection
