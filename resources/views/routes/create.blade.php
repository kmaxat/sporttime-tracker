@extends('layouts.app')
@include('layouts.navbar')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger hidden" id="errors">
                    <strong>Whoops! Something went wrong!</strong>
                    <br><br>
                    <ul id="errors-list">
                    </ul>
                </div>


                    <div class="row">
                        <div class="form-group col-md-6">
                            {!! Form::label('email', 'Название маршрута', array('class' => 'control-label')) !!}
                            {!! Form::text('title',Request::old('title'),array('id'=>'title', 'class' => 'form-control')) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="form-group col-md-10">
                                {!! Form::label('email', 'Маршрут', array('class' => 'control-label')) !!}
                            </div>
                            <div class="form-group col-md-10">
                                <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" onclick="deleteMarkers();" class="btn btn-default">Начать заново</button>
                                    <button type="button" onclick="popMarkers();" class="btn btn-danger">Удалить последную точку</button>
                                </div>

                                <h4 class="text-center">Дистанция <span id="distance"></span></h4>
                            </div>
                            <div class="form-group col-md-10">
                                <div style="width:100%; height:500px;" id="map"></div>
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="form-group col-md-6">
                            <button id="submit-route" type="submit" class="btn btn-success">
                                <i class="fa fa-plus"></i> Добавить маршрут
                            </button>
                        </div>
                    </div>


                <script>
                    var map;
                    var markers = [];
                    var poly;
                    var path = [];
                    var route = [];
                    var picture;
                    var distance;

                    function initMap() {
                        var location = {lat: 43.397, lng: 76.644};

                        map = new google.maps.Map(document.getElementById('map'), {
                            center: location,
                            zoom: 14
                        });


                        poly = new google.maps.Polyline({
                            strokeColor: '#1E90FF',
                            strokeOpacity: 1.0,
                            strokeWeight: 3
                        });
                        poly.setMap(map);


                        // Try HTML5 geolocation.
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                var pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude
                                };

                                map.setCenter(pos);
                            }, function() {
                                handleLocationError(true, map.getCenter());
                            });
                        } else {
                            // Browser doesn't support Geolocation
                            handleLocationError(false, map.getCenter());
                        }

                        map.addListener('click', function(event) {
                            addMarker(event.latLng);
                        });

                    }

                    function addMarker(location) {
                        path = poly.getPath();
                        path.push(location);

                        var marker = new google.maps.Marker({
                            position: location,
                            title: '#' + path.getLength(),
                            map: map
                        });
                        markers.push(marker);
                        location = {};
                        location.lat = marker.getPosition().lat();
                        location.lng = marker.getPosition().lng();

                        route.push(location);
                        printDistance(getDistance(poly));
                        getCenter(markers);
                    }

                    function setMapOnAll(map) {
                        for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(map);
                        }
                    }

                    function popMarkers() {
                        var lastMarker = markers.pop();
                        route.pop();
                        path.pop();
                        lastMarker.setMap(null);
                        printDistance(getDistance(poly));

                    }

                    function deleteMarkers() {
                        path = [];
                        poly.setPath(path);
                        setMapOnAll(null);
                        markers = [];
                        route = [];
                        printDistance(getDistance(poly));
                    }

                    function handleLocationError(browserHasGeolocation, pos) {
                        console.log('Error: The Geolocation service failed');
                    }

                    function getDistance(poly) {
                        polyLengthInMeters = google.maps.geometry.spherical.computeLength(poly.getPath().getArray());
                        distance = Math.round(polyLengthInMeters/1000 * 100) / 100;
                        return distance;
                    }

                    function printDistance(distance) {
                        document.getElementById('distance').innerHTML = getDistance(poly) + "км";
                        getDistance(poly);
                    }

                    function getCenter(markers) {
                        var bounds = new google.maps.LatLngBounds();
                        var i;
                        var path="path=color:0x0000ff|weight:3"
                        for (i = 0; i < markers.length; i++) {
                            bounds.extend(markers[i].getPosition());
                            path+="|"+markers[i].getPosition().toUrlValue();
                        }
                        picture = "https://maps.googleapis.com/maps/api/staticmap?" +
                        "center="+bounds.getCenter().toUrlValue()+"&size=1024x1024&format=jpg&maptype=roadmap&key=AIzaSyD3PUzj-kqVfJAZim2l7pCfHovOsREa-EA&"+
                        path;

                    }

                    $(document).ready(function(){
                        $('#submit-route').click(function(){
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN' : "{{ json_encode(csrf_token()) }}",
                                },
                                url: '/routes',
                                method: "POST",
                                data: {
                                    'title': $('#title').val(),
                                    'picture': picture,
                                    'distance': distance,
                                    'route' : JSON.stringify(route),
                                    '_token':"{{ csrf_token() }}"
                                },
                                success: function(result){
                                    window.location.replace("/routes?message="+$('#title').val());
                                },
                                error: function(data){
                                    var errors = $.parseJSON(data.responseText);
                                    $("#errors").removeClass("hidden").addClass("show");
                                    $.each(errors, function(index, value) {
                                        $("#errors-list").empty();
                                        $.each(value[0], function( index, value ) {
                                            $("#errors-list").append("<li>"+value+"</li>");
                                        });
                                    });
                                }

                            });
                        });
                    });
                </script>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3PUzj-kqVfJAZim2l7pCfHovOsREa-EA&libraries=geometry&callback=initMap" async defer></script>
            </div>
        </div>
    </div>
@endsection
