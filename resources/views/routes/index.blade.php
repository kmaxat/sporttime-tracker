@extends('layouts.app')
@include('layouts.navbar')
@section('content')
    <div class="container">
        @if(isset($message))
        <div class="alert alert-success" role="alert"><a href="#" class="alert-link">Маршрут "{{$message}}" добавлен</a></div>
        @endif

        <div class="row" style="padding-bottom: 20px">
            <div class="col-md-offset-10 col-md-2">
                <a class="btn btn-success" href="{{url('routes/create')}}" role="button">Добавить маршрут</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Название</th>
                            <th>Дистанция (км)</th>
                            <th>Карта</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0 ?>
                        @foreach ($routes as $route)
                            <?php $i ++ ?>
                            <tr>
                                <th scope="row">{{$i}}</th>
                                <td>{{$route->title}}</td>
                                <td>{{$route->distance}}</td>
                                <td>
                                    <img style="height: 150px;" src="{{asset($route->picture)}}">
                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="...">
                                        <a class="btn btn-default" href="{{url('/routes/'.$route->id.'/edit')}}" role="button">Изменить</a>
                                        <button type="submit" id="{{ $route->id }}" class="btn btn-danger">
                                            Удалить
                                        </button>
                                    </div>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    <script>
        $('.btn-danger').on('click', function(){
            var id = $(this).attr('id');
            swal({
                        title: "Удалить маршрут?",
                        text: "Все данные ассоциированные с этим маршрутом будут удалены безвозвратно",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, удалить",
                        cancelButtonText: "Отмена",
                        closeOnConfirm: false
                    },
                    function(){
                        $.ajax({
                            url: '/routes/'+ id,
                            method: "POST",
                            data: {
                                '_method'   :   "delete",
                                '_token'    :   "{{ csrf_token() }}"
                            },
                            success: function(result){
                                window.location.replace("/routes");
                            },
                            error: function(data){
                                console.log(data);
                            }

                        });
                    });
        })
    </script>
@endsection
