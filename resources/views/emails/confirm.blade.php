<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Подтверждение емайла</title>
</head>
<body>
<h3>Добро пожаловать в <a href="http://www.sporttimeapp.com">SportTime Club</a>!</h3>

<p>
    Для подтверждения емайла нажмите на ссылку <a href='{{ url(env('API_PREFIX').'/'.env('API_VERSION')."/auth/register/confirm/{$user->token}") }}'>подтвердить</a>.
    <br>
    Вы также можете скопировать данную ссылку в адресную строку в браузере.<br>
    {{ url(env('API_PREFIX').'/'.env('API_VERSION')."/auth/register/confirm/{$user->token}") }}
</p>
</body>
</html>