@extends('layouts.app')
@include('layouts.navbar')
@section('content')
    <div class="container">
        @if(isset($message))
        <div class="alert alert-success" role="alert"><a href="#" class="alert-link">{{$message}}</a></div>
        @endif

        <div class="row" style="padding-bottom: 20px">
            <div class="col-md-offset-10 col-md-2">
                <a class="btn btn-success" href="{{url('sports/create')}}" role="button">Добавить вид спорта</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Название</th>
                            <th>Название (анл.яз)</th>
                            <th>Кал/ч</th>
                            <th>Изображение</th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0 ?>
                        @foreach ($sports as $sport)
                            <?php $i ++ ?>
                            <tr>
                                <th scope="row">{{$i}}</th>
                                <td>{{$sport->title}}</td>
                                <td>{{$sport->title_eng}}</td>
                                <td>{{$sport->calories}}</td>
                                <td>
                                    <img style="width: 72px; height:72px" src="{{asset($sport->picture)}}">
                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="...">
                                        <a class="btn btn-default" href="{{url('/sports/'.$sport->id.'/edit')}}" role="button">Изменить</a>
                                        <button type="submit" id="{{ $sport->id }}" class="btn btn-danger">
                                            Удалить
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    <script>
        $('.btn-danger').on('click', function(){
            var id = $(this).attr('id');
            swal({
                        title: "Удалить вид спорта?",
                        text: "Все данные ассоциированные с этим видом спорта будут удалены безвозвратно",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, удалить",
                        cancelButtonText: "Отмена",
                        closeOnConfirm: false
                    },
                    function(){
                        $.ajax({
                            url: '/sports/'+ id,
                            method: "POST",
                            data: {
                                '_method'   :   "delete",
                                '_token'    :   "{{ csrf_token() }}"
                            },
                            success: function(result){
                                window.location.replace("/sports");
                            },
                            error: function(data){
                                console.log(data);
                            }

                        });
                    });
        })
    </script>
@endsection
