@extends('layouts.app')
@include('layouts.navbar')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @if (count($errors) > 0)
                        <!-- Form Error List -->
                <div class="alert alert-danger">
                    <strong>Whoops! Something went wrong!</strong>

                    <br><br>

                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {!! Form::open([
                    'route' => array('sports.update', $sport->id),
                    'class' => 'form-horizontal',
                    'files' => true,
                    'method' => 'PUT',
                    ]
                ) !!}
                <div class="form-group">
                    {!! Form::label('email', 'Название спорта', array('class' => 'col-sm-3 control-label')) !!}
                    <div class="col-sm-6">
                        {!! Form::text('title', $sport->title , array('class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Название спорта на английском', array('class' => 'col-sm-3 control-label')) !!}
                    <div class="col-sm-6">
                        {!! Form::text('title_eng', $sport->title_eng, array('class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Кол-во калорий в час', array('class' => 'col-sm-3 control-label')) !!}
                    <div class="col-sm-6">
                        {!! Form::text('calories', $sport->calories, array('class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Иконка спорта', array('class' => 'col-sm-3 control-label')) !!}
                    <div class="col-sm-4">
                        {!! Form::file('picture', null) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-sm-4 ">
                        <img style="width: 250px" src="{{asset($sport->picture)}}">
                    </div>
                </div>

                <!-- Add Task Button -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-plus"></i> Сохранить
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
