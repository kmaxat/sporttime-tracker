@extends('layouts.app')
@include('layouts.navbar')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Пользователь</th>
                            <th>Вид спорта</th>
                            <th>Широта, Долгота</th>
                            <th>Название</th>
                            <th>Закрыта</th>
                            <th>Описани</th>
                            <th>Дата старта</th>
                            <th>Время старта</th>
                            <th>Время окончания</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $page ==1 ? $i =1 : $i = $page*15-14?>
                        @foreach ($grouptrainings as $grouptraining)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$grouptraining->user->email}}</td>
                                <td>{{$grouptraining->sport->title}}</td>
                                <td>{{$grouptraining->location['lat'].','.$grouptraining->location['lon']}}</td>
                                <td>{{$grouptraining->title}}</td>
                                <td>{{$grouptraining->private_state}}</td>
                                <td>{{ str_limit($grouptraining->description, $limit = 100, $end = '...')}}</td>
                                <td>{{$grouptraining->start_date}}</td>
                                <td>{{$grouptraining->start_time}}</td>
                                <td>{{$grouptraining->end_time}}</td>
                                <td>
                                    <button type="submit" id="{{ $grouptraining->id }}" class="btn btn-danger">
                                        Удалить
                                    </button>
                                </td>
                            </tr>
                            <?php $i ++ ?>

                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    {!! $grouptrainings->links() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.btn-danger').on('click', function(){
            var id = $(this).attr('id');
            swal({
                        title: "Удалить групповую тренировку?",
                        text: "Все данные ассоциированные с этой тренировкой будут удалены безвозвратно",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, удалить",
                        cancelButtonText: "Отмена",
                        closeOnConfirm: false
                    },
                    function(){
                        $.ajax({
                            url: '/grouptrainings/'+ id,
                            method: "POST",
                            data: {
                                '_method'   :   "delete",
                                '_token'    :   "{{ csrf_token() }}"
                            },
                            success: function(result){
                                window.location.replace("/grouptrainings");
                            },
                            error: function(data){
                                console.log(data);
                            }

                        });
                    });
        })
    </script>
@endsection
