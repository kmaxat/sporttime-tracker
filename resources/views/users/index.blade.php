@extends('layouts.app')
@include('layouts.navbar')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Роль</th>
                                <th>Email</th>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Поль</th>
                                <th>День рождения</th>
                                <th>Подтвержденный аккаунт</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php $page == 1 ? $i =1 : $i = $page*15-14?>
                        @foreach ($users as $user)
                            <tr>
                                <th scope="row">{{$i}}</th>
                                <td>{{$user->role}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->first_name}}</td>
                                <td>{{$user->last_name}}</td>
                                <td>@if($user->gender == 'male')
                                        Мужской
                                    @else
                                        Женский
                                    @endif
                                </td>
                                <td>{{$user->birthday}}</td>
                                <td>@if($user->verified)
                                        Да
                                    @else
                                        Нет
                                    @endif
                                </td>
                                <td>
                                    <button id="{{$user->id}}" class="btn btn-danger">
                                        Удалить
                                    </button>
                            </tr>
                            <?php $i ++ ?>

                        @endforeach

                        </tbody>
                    </table>

                </div>
                <div class="text-center">
                    {!! $users->links() !!}
                </div>
              </div>
        </div>
    </div>
    <script>
        $('.btn-danger').on('click', function(){
            var id = $(this).attr('id');
            swal({
                        title: "Удалить пользователя?",
                        text: "Все данные ассоциированные с этим пользователем будут удалены безвозвратно",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, удалить",
                        cancelButtonText: "Отмена",
                        closeOnConfirm: false
                    },
                    function(){
                        $.ajax({
                            url: '/users/'+ id,
                            method: "POST",
                            data: {
                                '_method'   :   "delete",
                                '_token'    :   "{{ csrf_token() }}"
                            },
                            success: function(result){
                                window.location.replace("/users");
                            },
                            error: function(data){
                                console.log(data);
                            }

                        });
                    });
        })
    </script>
@endsection
