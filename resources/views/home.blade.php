@extends('layouts.app')
@include('layouts.navbar')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">Зарегистрированных пользователей</div>

                    <div class="panel-body">
                        {{$users}}
                    </div>

                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">Созданных групповых тренировок</div>

                    <div class="panel-body">
                        {{$grouptrainings}}
                    </div>

                </div>

                <div class="panel panel-warning">
                    <div class="panel-heading">Видов спорта</div>

                    <div class="panel-body">
                        {{$sports}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
